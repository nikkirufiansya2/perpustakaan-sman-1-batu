<?php 
if ($this->session->flashdata('sukses')) {
	echo '<div class="alert alert-success">';
	echo $this->session->flashdata('sukses');
	echo '</div>';
}

//error form
echo validation_errors('<div class = alert alert-warning">', '</div>');

//form open
echo form_open(base_url('admin/konfigurasi'));
?>

<div class="col-md-6">
	
<div class="form-group">
	<label>Nama Website</label>
	<input type="text" name="namaweb" placeholder="Nama Website" value="<?php echo $konfigurasi->namaweb ?>" class="form-control" required>
</div>

<div class="form-group">
	<label>Tagline</label>
	<input type="text" name="tagline" placeholder="Tagline Website" value="<?php echo $konfigurasi->tagline ?>" class="form-control" >
</div>

<div class="form-group">
	<label>Email Resmi</label>
	<input type="text" name="email" placeholder="Email Resmi " value="<?php echo $konfigurasi->email ?>" class="form-control" >
</div>

<div class="form-group">
	<label>Website</label>
	<input type="url" name="website" placeholder="<?php echo base_url() ?>	" value="<?php echo $konfigurasi->website ?>" class="form-control">
</div>

<div class="form-group">
	<label>Facebook Account</label>
	<input type="url" name="facebook" placeholder="http://facebook.com/akun" value="<?php echo $konfigurasi->facebook ?>" class="form-control">
</div>

<div class="form-group">
	<label>Twitter Account</label>
	<input type="url" name="twitter" placeholder="http://twitter.com/akun" value="<?php echo $konfigurasi->twitter ?>" class="form-control">
</div>

<div class="form-group">
	<label>Instagram Account</label>
	<input type="url" name="instagram" placeholder="http://instagram.com/akun" value="<?php echo $konfigurasi->instagram ?>" class="form-control">
</div>

<div class="form-group">
	<label>Telepon </label>
	<input type="text" name="telepon" placeholder="Nomor telepon" value="<?php echo $konfigurasi->telepon ?>" class="form-control">
</div>

<div class="alert alert-success">
<h3>Setting Peminjaman Buku</h3>
<hr>
<div class="form-group">
	<label>Durasi Maksimal Peminjaman Buku </label>
	<input type="number" name="max_hari_peminjaman" placeholder="Hari Maksimal Peminjaman Buku" value="<?php echo $konfigurasi->max_hari_peminjaman ?>" class="form-control">
</div>

<div class="form-group">
	<label>Jumlah Maksimal Peminjaman Buku </label>
	<input type="number" name="max_jumlah_peminjaman" placeholder="Jumlah Maksimal Peminjaman Buku " value="<?php echo $konfigurasi->max_jumlah_peminjaman ?>" class="form-control">
</div>

<div class="form-group">
	<label>Denda per hari (Rp)</label>
	<input type="number" name="denda_perhari" placeholder="Denda per hari " value="<?php echo $konfigurasi->denda_perhari ?>" class="form-control">
</div>
</div>

</div>

<div class="col-md-6">

	<div class="form-group">
	<label>Alamat Perpustakaan</label>
	<textarea name="alamat" class="form-control" placeholder="Alamat Perpustakaan"> <?php echo $konfigurasi->alamat ?></textarea>
	</div>

	<div class="form-group">
	<label>Deskripsi Website</label>
	<textarea name="deskripsi" class="form-control" placeholder="Deskripsi Website"> <?php echo $konfigurasi->deskripsi ?></textarea>
	</div>

	<div class="form-group">
	<label>Keywords Website</label>
	<textarea name="keywords" class="form-control" placeholder="Keywords Website"> <?php echo $konfigurasi->keywords ?></textarea>
	</div>

	<div class="form-group">
	<label>Google Maps (format iframe)</label>
	<textarea name="map" class="form-control" placeholder="Google Maps"> <?php echo $konfigurasi->map  ?></textarea>
	</div>

	<div class="form-group">
	<label>Metatext ( Biasanya dari Google analitys &amp; Webmaster )</label>
	<textarea name="metatext" class="form-control" placeholder="GMetatext ( Biasanya dari Google analitys &amp; Webmaster )"> <?php echo $konfigurasi->metatext  ?></textarea>
	</div>

	<div class="form-group">
		<button type="submit" class="btn btn-success btn-lg" name="submit">
		<i class="fa fa-save"></i> Simpan
		</button>

		<button type="reset" class="btn btn-default btn-lg" name="reset">
		<i class="fa fa-times"></i> Reset
		</button>

	</div>

</div>

<?php
// form close
echo form_close();
?>