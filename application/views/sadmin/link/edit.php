<?php
//notif input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>  ', '</div>');

//buka form
echo form_open(base_url('admin/link/edit/'. $link->id_link));
?>

<div class="col-md-12">
	<div class="form-group">
	<label>Nama Link</label>
	<input type="text" name="nama_link" class="form-control" placeholder="Nama Jenis Buku" value="<?php echo $link->nama_link ?>" required>		
	</div>	


	<div class="form-group">
		<label>URL</label>
		<input type="url" name="url" placeholder="<?php echo base_url() ?>" value="<?php echo $link->url?>" class="form-control">
	</div> 

	<div class="form-group">
		<label>Target</label>
		<select class="form-control" name="target">
			<option value="_blank">_blank</option>
			<option value="_self" <?php if($link->target=="_self") { echo "selected";}  ?>>_self</option>
			<option value="_parent"<?php if($link->target=="_parent") { echo "selected";}  ?>>_parent</option>
			<option value="_top"<?php if($link->target=="_top") { echo "selected";}  ?>>_top</option>
		</select>
	</div> 

	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
		<input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
	</div>

</div>


<?php
//tutup form
echo form_close();
?>