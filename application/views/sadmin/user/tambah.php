<?php
//notif input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>  ', '</div>');

//buka form
echo form_open(base_url('admin/user/tambah'));
?>

<div class="col-md-6">
	<div class="form-group">
	<label>Nama</label>
	<input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="<?php echo set_value('nama') ?>" required>		
	</div>	

	<div class="form-group">
	<label>Email</label>
	<input type="email" name="email" class="form-control" placeholder="email anda" value="<?php echo set_value('email') ?>" required>		
	</div>

	<div class="form-group">
	<label>Uasername</label>
	<input type="text" name="username" class="form-control" placeholder="username anda" value="<?php echo set_value('username') ?>" required>		
	</div>

	<div class="form-group">
	<label>Password</label>
	<input type="password" name="password" class="form-control" placeholder="password" value="<?php echo set_value('password') ?>" required>		
	</div>
</div>

<div class="col-md-6">
	<div class="form-group">
		<label>Hak Akses</label>
		<select name="akses_level" class="form-control">
			<option value="admin">Admin</option>
			<option value="user">User</option>
		</select>
	</div>

	<div class="form-group">
	<label>Keterangan</label>
	<textarea name="keterangan" class="form-control" placeholder="Keterangan"><?php echo set_value('keterangan')?></textarea>	
	</div>

	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
		<input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
	</div>

</div>


<?php
//tutup form
echo form_close();
?>