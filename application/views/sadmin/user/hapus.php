<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#Hapus<?php echo $user->id_user?>">
    <i class="fa fa-trash-o"></i> Hapus
</button>
<div class="modal fade" id="Hapus<?php echo $user->id_user?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Hapus Data : <?php echo $user->nama?></h4>
        </div>
        <div class="modal-body">
            <p class="alert alert-danger"><i class="fa fa-warning" > Apakah anda yakin untuk menghapus data ini ?</i></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>           
            
            <a href="<?php  echo base_url('admin/user/hapus/' .$user->id_user)?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Hapus</a>

        </div>
    </div>
</div>
</div>