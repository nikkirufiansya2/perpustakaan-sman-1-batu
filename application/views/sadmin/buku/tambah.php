<?php
//notif input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>  ', '</div>');


//error upload cover
if (isset($error)) {
	echo '<div class = "alert alert-warning">';
	echo $error;
	echo '</div>';
}
//buka form
echo form_open_multipart(base_url('admin/buku/tambah'));
?>

<div class="col-md-12">
	<div class="form-group">
	<label>Judul Buku</label>
	<input type="text" name="judul_buku" class="form-control" placeholder="Judul Buku" value="<?php echo set_value('judul_buku') ?>" required>		
	</div>	
</div>

<div class="col-md-4">
	<div class="form-group">
	<label>Penulis Buku</label>
	<input type="text" name="penulis_buku" class="form-control" placeholder="Penulis Buku" value="<?php echo set_value('penulis_buku') ?>" required>		
	</div>

	<div class="form-group">
	<label>Kode Buku</label>
	<input type="text" name="kode_buku" class="form-control" placeholder="Kode Buku" value="<?php echo set_value('kode_buku') ?>">		
	</div>

	<div class="form-group">
	<label>Nomor Seri Buku</label>
	<input type="text" name="nomor_seri" class="form-control" placeholder="Nomor Seri Buku" value="<?php echo set_value('nomor_seri') ?>">		
	</div>

	<div class="form-group">
	<label>Jenis Buku</label>
	<select name="id_jenis" class="form-control">
		<?php foreach ($jenis as $jenis) { ?>
			<option value="<?php echo $jenis->id_jenis ?>">
				<?php echo $jenis->nama_jenis ?>
			</option>
		<?php } ?>
	</select>		
	</div>

	<div class="form-group">
	<label>Bahasa Buku</label>
	<select name="id_bahasa" class="form-control">
		<?php foreach ($bahasa as $bahasa) { ?>
			<option value="<?php echo $bahasa->id_bahasa ?>">
				<?php echo $bahasa->nama_bahasa ?>
			</option>
		<?php } ?>
	</select>		
	</div>

</div>

<div class="col-md-4">
	<div class="form-group">
	<label>Berat Buku <span class="text-danger"> <small>(gram)</small></span></label>
	<input type="number" name="kolasi" class="form-control" placeholder="Berat Buku" value="<?php echo set_value('kolasi') ?>">		
	</div>

	<div class="form-group">
	<label>Penerbit Buku</label>
	<input type="text" name="penerbit" class="form-control" placeholder="Penerbit Buku" value="<?php echo set_value('penerbit') ?>">		
	</div>

	<div class="form-group">
	<label>Tahun Terbit</label>
	<input type="number" name="tahun_terbit" class="form-control" placeholder="Tahun Terbit Buku" value="<?php echo set_value('tahun_terbit') ?>">		
	</div>

	<div class="form-group">
		<label>Status Buku</label>
		<select name="akses_level" class="form-control">
			<option value="Publish">Publish</option>
			<option value="Not Publish">Not Publish</option>
			<option value="Missing">Missing</option>
		</select>
	</div>

	<div class="form-group">
	<label>Deskripsi Buku</label>
	<textarea name="ringkasan" class="form-control" placeholder="Deskripsi Buku"><?php echo set_value('ringkasan')?></textarea>	
	</div>
</div>

<div class="col-md-4">
	<div class="form-group">
	<label>Jumlah Halaman</label>
	<input type="number" name="subjek_buku" class="form-control" placeholder="Jumlah Halaman Buku" value="<?php echo set_value('subjek_buku') ?>">		
	</div>

	<div class="form-group">
	<label>Letak Buku</label>
	<input type="text" name="letak_buku" class="form-control" placeholder="Letak Buku" value="<?php echo set_value('letak_buku') ?>">		
	</div>

	<div class="form-group">
	<label>Jumlah Buku</label>
	<input type="number" name="jumlah_buku" class="form-control" placeholder="Jumlah Buku" value="<?php echo set_value('jumlah_buku') ?>">		
	</div>

	<div class="form-group">
	<label>Cover Buku</label>
	<input type="file" name="cover_buku" class="form-control" placeholder="Cover Buku" value="<?php echo set_value('cover_buku') ?>">		
	</div>
</div>

<div class="col-md-12 text-center">
	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
		<input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
	</div>

</div>


<?php
//tutup form
echo form_close();
?>