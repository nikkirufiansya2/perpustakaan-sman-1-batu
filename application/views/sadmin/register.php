<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="<?php echo base_url() ?>assets/img/basic/pin.png" type="image/x-icon">
    <title><?php echo $title ?></title>
    <!-- CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/css/app.css">
        <style>
            .loader {
                position: fixed;
                left: 0;
                top: 0;
                width: 100%;
                height: 100%;
                background-color: #F5F8FA;
                z-index: 9998;
                text-align: center;
            }

            .plane-container {
                position: absolute;
                top: 50%;
                left: 50%;
            }
        </style>
    <!-- Js -->
    <script>(function(w,d,u){w.readyQ=[];w.bindReadyQ=[];function p(x,y){if(x=="ready"){w.bindReadyQ.push(y);}else{w.readyQ.push(x);}};var a={ready:p,bind:p};w.$=w.jQuery=function(f){if(f===d||f===u){return a}else{p(f)}}})(window,document)</script>
</head>
<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left"><div class="circle"></div></div>
                <div class="gap-patch"><div class="circle"></div></div>
                <div class="circle-clipper right"><div class="circle"></div></div>
            </div>
            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left"><div class="circle"></div></div>
                <div class="gap-patch"><div class="circle"></div></div>
                <div class="circle-clipper right"><div class="circle"></div></div>
            </div>
            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left"><div class="circle"></div></div>
                <div class="gap-patch"><div class="circle"></div></div>
                <div class="circle-clipper right"><div class="circle"></div></div>
            </div>
            <div class="spinner-layer spinner-green">
                <<div class="circle-clipper left"><div class="circle"></div></div>
                <div class="gap-patch"><div class="circle"></div></div>
                <div class="circle-clipper right"><div class="circle"></div></div>
            </div>
        </div>
    </div>
</div>
<div id="app">
    <main>
        <div id="primary" class="p-t-b-100 height-full">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 mx-md-auto">
                        <div class="shadow r-10">
                        <div class="row grid">
                            <div class="col-md-5 white p-5">
                               <div class="mb-5">
                                   <img src="<?php echo base_url() ?>assets/img/basic/logo.png" width="1000" alt="">
                               </div>
                               <P>Silahkan Masukkan Username dan Password anda !</P>
                               <hr>
                               <br>

                               <?php
                                //notif input error
                               echo validation_errors('<div class = "alert alert-danger"><i class="fa fa-warning"></i>', '</div' );

                                //notif 
                                if($this->session->flashdata('sukses')){
                                    echo '<div class ="alert alert-success"><i class="fa fa-check"></i>';
                                    echo $this->session->flashdata('sukses');
                                    echo '</div>';
                                } 
                               ?>
                                <form class="form-material" action="<?php echo site_url('register/tambahuser') ?>" method="post">
                                    <div class="body">
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="nama"/>
                                                <label class="form-label">Nama</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="email" class="form-control" name="email"/>
                                                <label class="form-label">Email</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="text" class="form-control" name="username"/>
                                                <label class="form-label">Username</label>
                                            </div>
                                        </div>
                                        <div class="form-group form-float">
                                            <div class="form-line">
                                                <input type="password" class="form-control" name="password"/>
                                                <label class="form-label">Password</label>
                                            </div>  
                                        </div>
                                        <input type="submit" name="submit" class="btn btn-primary btn-sm pl-4 pr-4" value="Register">
                                        <div class="pt-5 pb-5"></div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-7 blue p-5 text-white">
                                <h2 class="bold"><i class="icon-book s-36"></i> Perpustakaan</h2>
                            <div class="pt-3 mb-5">
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            </div>
                                <a href='<?php echo site_url("login") ?>' class="btn btn-info s-14 pl-4 pr-4"> Sudah Punya Akun</a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #primary -->
    </main>
    <!-- Add the sidebar's background. This div must be placed immediately after the control sidebar -->
    <div class="control-sidebar-bg shadow white fixed"></div>
</div>
<script src="<?php echo base_url() ?>assets/js/app.js"></script>
<script>(function($,d){$.each(readyQ,function(i,f){$(f)});$.each(bindReadyQ,function(i,f){$(d).bind("ready",f)})})(jQuery,document)</script>
</body>
</html>