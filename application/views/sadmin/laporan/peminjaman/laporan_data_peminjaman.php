<h2><center>Laporan Data Peminjaman Buku</center></h2>
<hr/>
<table border="1" width="100%" style="text-align: center;">
	<tr>
		<th>No</th>
		<th>Nama Anggota</th>
		<th>Judul Buku</th>
		<th>Tanggal Pinjam</th>
		<th>Tanggal Kembali</th>
		<th>Status Kembali</th>
	</tr>
	<?php $i=1; foreach($peminjaman as $peminjaman) { ?>
		<tr>
			<td><?php echo $i ?></td>
			<td><?php echo $peminjaman->nama_anggota?></td>
			<td><?php echo $peminjaman->judul_buku?></td>
			<td><?php echo date('d-m-Y', strtotime($peminjaman->tanggal_pinjam)) ?> </td>
			<td><?php echo date('d-m-Y', strtotime($peminjaman->tanggal_kembali)) ?> </td>
			<td><?php echo $peminjaman->status_kembali?> </td>
			
		</tr>
		<?php $i++; } ?>
	</table>