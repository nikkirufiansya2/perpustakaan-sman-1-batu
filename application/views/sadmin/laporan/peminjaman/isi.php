<p><a target="_blank" href="<?php echo base_url('admin/laporan/cetak_data_peminjaman') ?>" class ="btn btn-success">
    <i class="fa fa-print"></i> Print</a></p>

<?php
//notif
if($this->session->flashdata('sukses')){
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Nama Anggota</th>
            <th>Judul Buku</th>
            <th>Tanggal Pinjam</th>
            <th>Tanggal Kembali</th>
            <th>Status Kembali</th>
           
        </tr>
    </thead>
    <tbody>
    <?php $i=1; foreach($peminjaman as $peminjaman) { ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $peminjaman->nama_anggota?></td>
            <td><?php echo $peminjaman->judul_buku?></td>
            <td><?php echo date('d-m-Y', strtotime($peminjaman->tanggal_pinjam)) ?> </td>
            <td><?php echo date('d-m-Y', strtotime($peminjaman->tanggal_kembali)) ?> </td>
            <td><?php echo $peminjaman->status_kembali?> </td>
           
        </tr>
    <?php $i++; } ?>
    </tbody>
</table>