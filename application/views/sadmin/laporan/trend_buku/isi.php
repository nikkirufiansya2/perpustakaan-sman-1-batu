<p><a target="_blank" href="<?php echo base_url('admin/laporan/cetak_data_trenBuku') ?>" class ="btn btn-success">
    <i class="fa fa-print"></i> Print</a></p>

<?php
//notif
if($this->session->flashdata('sukses')){
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Judul Buku</th>
            <th>Penulis</th>
            <th>Penerbit</th>
            <th>Total Peminjaman</th>
           
           
        </tr>
    </thead>
    <tbody>
    <?php $i=1; foreach($laporan_trend_buku as $buku) { ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $buku->judul_buku?></td>
            <td><?php echo $buku->penulis_buku?></td>
            <td><?php echo $buku->penerbit?></td>
            <td><?php echo $buku->total_peminjaman?></td>
            
        </tr>
    <?php $i++; } ?>
    </tbody>
</table>