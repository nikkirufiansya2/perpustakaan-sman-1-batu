<h2><center>Laporan data tren Buku</center></h2>
<hr/>
<table border="1" width="100%" style="text-align: center;">
	<tr>
		<th>No</th>
		<th>Judul Buku</th>
		<th>Penulis</th>
		<th>Penerbit</th>
		<th>Total Peminjaman</th>
	</tr>
	<?php 
	$no = 1;
	foreach($trend_buku as $buku)
	{
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $buku->judul_buku?></td>
			<td><?php echo $buku->penulis_buku?></td>
			<td><?php echo $buku->penerbit?></td>
			<td><?php echo $buku->total_peminjaman?></td>
		</tr>
		<?php
	}
	?>
</table>