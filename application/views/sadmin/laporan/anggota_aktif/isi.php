<p><a target="_blank" href="<?php echo base_url('admin/laporan/cetak_anggota_aktif') ?>" class ="btn btn-success">
    <i class="fa fa-print"></i> Print</a></p>

<?php
//notif
if($this->session->flashdata('sukses')){
    echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
    echo $this->session->flashdata('sukses');
    echo '</div>';
}
?>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Nama Anggota</th>
            <th>Email - Telepon</th>
            <th>Username - Status</th>
            <th>Instansi</th>
           
        </tr>
    </thead>
    <tbody>
    <?php $i=1; foreach($laporan_anggota_aktif as $anggota) { ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $anggota->nama_anggota?></td>
            <td><?php echo $anggota->email?>
                <br><i class="fa fa-phone"></i> <?php echo $anggota->telepon?>
            </td>
            <td><?php echo $anggota->username?> - <?php echo $anggota->status_anggota?></td>
            <td><?php echo $anggota->instansi?></td>
            
        </tr>
    <?php $i++; } ?>
    </tbody>
</table>