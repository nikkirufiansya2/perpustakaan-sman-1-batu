<h2><center>Laporan Anggota Aktif</center></h2>
<hr/>
<table border="1" width="100%" style="text-align: center;">
	<tr>
		<th>No</th>
		<th>Nama Anggota</th>
		<th>Email - Telepon</th>
		<th>Username - Status</th>
		<th>Instansi</th>
	</tr>
	<?php 
	$no = 1;
	foreach($anggota_aktif as $anggota)
	{
		?>
		<tr>
			<td><?php echo $no++ ?></td>
			<td><?php echo $anggota->nama_anggota?></td>
			<td><?php echo $anggota->email?> - <?php echo $anggota->telepon?>
		</td>
		<td><?php echo $anggota->username?> - <?php echo $anggota->status_anggota?></td>
		<td><?php echo $anggota->instansi?></td>
	</tr>
	<?php
}
?>
</table>