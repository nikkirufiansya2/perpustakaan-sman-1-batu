<p class="alert alert-warning">
    <i class="fa fa-warning"></i> Cari nama anggota, kemudian klik tombol <strong><i class="fa fa-plus"></i> Peminjaman</strong>
</p>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Nama Anggota</th>
            <th>Email - Telepon</th>
            <th>Username - Status</th>
            <th>Instansi</th>
            <th width="15%">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; foreach($anggota as $anggota) { ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $anggota->nama_anggota?></td>
            <td><?php echo $anggota->email?>
                <br><i class="fa fa-phone"></i> <?php echo $anggota->telepon?>
            </td>
            <td><?php echo $anggota->username?> - <?php echo $anggota->status_anggota?></td>
            <td><?php echo $anggota->instansi?></td>
            <td>
                <a href="<?php echo base_url('admin/peminjaman/add/' . $anggota->id_anggota) ?>" class = "btn btn-primary btn-xs"><i class="fa fa-plus"></i>  Peminjaman</a>
                
            </td>
        </tr>
    <?php $i++; } ?>
    </tbody>
</table>