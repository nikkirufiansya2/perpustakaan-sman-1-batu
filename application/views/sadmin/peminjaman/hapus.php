<button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#Hapus<?php echo $peminjaman->id_peminjaman ?>">
    <i class="fa fa-trash-o"></i> Hapus
</button>
<div class="modal fade" id="Hapus<?php echo $peminjaman->id_peminjaman ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
            <h4 class="modal-title" id="myModalLabel">Hapus  Peminjaman </h4>
        </div>
        <div class="modal-body">
            <p class="alert alert-danger"><i class="fa fa-warning" > Apakah anda yakin untuk menghapus data ini ?</i></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>           
            
            <a href="<?php  echo base_url('admin/peminjaman/hapus/' .$peminjaman->id_peminjaman)?>" class="btn btn-danger"><i class="fa fa-trash-o"></i> Hapus</a>

        </div>
    </div>
</div>
</div>