<?php if($this->uri->segment(3) == "") { ?>
    <p><a href="<?php echo base_url('admin/peminjaman/tambah') ?>" class ="btn btn-success">
        <i class="fa fa-plus"></i>Tambah</a></p>
    <?php } ?>

    <?php
//notif
    if($this->session->flashdata('sukses')){
        echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
        echo $this->session->flashdata('sukses');
        echo '</div>';
    }
    ?>

    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
        <thead>
            <tr>
                <th>#</th>
                <th>Nama Anggota</th>
                <th>Judul Buku</th>
                <th>Tanggal Pinjam</th>
                <th>Tanggal Kembali</th>
                <th>Status Kembali</th>
                <th>Denda</th>
                <th width="22%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $i=1; foreach($peminjaman as $peminjaman) { ?>
                <tr>
                    <td><?php echo $i ?></td>
                    <td>
                        <a href="<?php echo base_url('admin/peminjaman/add/'.$peminjaman->id_anggota) ?>">
                            <?php echo $peminjaman->nama_anggota?> <sup><i class="fa fa-link"></i></sup>
                        </a>
                    </td>
                    <td><?php echo $peminjaman->judul_buku?></td>
                    <td><?php echo date('d-m-Y', strtotime($peminjaman->tanggal_pinjam)) ?> </td>
                    <td><?php echo date('d-m-Y', strtotime($peminjaman->tanggal_kembali)) ?> </td>
                    <td><?php echo $peminjaman->status_kembali?> </td>
                    <td><?php 

                    $pecah = explode("-", $peminjaman->tanggal_kembali);
                    $pecah2 = explode("/", date("Y/m/d"));
                    $datepinjam = $pecah[2]."-".$pecah[1]."-".$pecah[0];
                    $datekembali = $pecah2[2]."-".$pecah2[1]."-".$pecah2[0];
                    $tgl1 = new DateTime($datepinjam);
                    $tgl2 = new DateTime($datekembali);

                    $d = $tgl2->diff($tgl1)->days;
                    if(($pecah[2]<$pecah2[2]) || ($pecah[1]<$pecah2[1]) || ($pecah[0]<$pecah2[0]) ){
                       if($peminjaman->status_kembali == "belum"){
                        foreach ($denda as $total ) {
                            echo "Rp.".($d* $total->denda);
                        }
                        
                    }else{

                    }
                }
                else{
                    echo 0;
                }
                ?> </td>
                <td>
                    <a href="<?php echo base_url('admin/peminjaman/edit/' . $peminjaman->id_peminjaman) ?>" class = "btn btn-warning btn-xs"><i class="fa fa-pencil"></i>  Edit</a>

                    <?php include('kembali.php'); ?>

                    <?php include('hapus.php'); ?>

                </td>
            </tr>
            <?php $i++; } ?>
        </tbody>
    </table>