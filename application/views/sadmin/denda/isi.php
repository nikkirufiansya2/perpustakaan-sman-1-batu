<table class="table table-striped table-bordered table-hover" id="dataTables-example">
	 <thead>
        <tr>
            <th>No</th>
            <th>Total Denda</th>
            <th width="15%">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php $i=1; foreach($denda as $data) { ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $data->denda?></td>
            <td>
                <a href="<?php echo base_url('admin/denda/edit/' . $data->id) ?>" class = "btn btn-warning btn-xs"><i class="fa fa-pencil"></i>Edit</a>
                

            </td>
        </tr>
    <?php $i++; } ?>
    </tbody>
</table>