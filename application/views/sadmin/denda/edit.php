<?php
//notif input error
echo validation_errors('<div class="alert alert-danger"><i class="fa fa-warning"></i>  ', '</div>');

//buka form
echo form_open(base_url('admin/denda/edit/'. $denda->id));
?>

<div class="col-md-6">
	<div class="form-group">
	<label>Denda</label>
	<input type="text" name="denda" class="form-control" placeholder="Denda" value="<?php echo $denda->denda ?>" required>		
	</div>	

	
	<div class="form-group">
		<input type="submit" name="submit" class="btn btn-success btn-lg" value="Simpan">
		<input type="reset" name="reset" class="btn btn-default btn-lg" value="Reset">
	</div>

</div>


<?php
//tutup form
echo form_close();
?>