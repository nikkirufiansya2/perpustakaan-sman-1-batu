<?php
//perlindungan halaman 

if ($this->session->userdata('username')== "" && $this->session->userdata('akses_level')=="") {
	$this->session->set_flashdata('sukses','Anda Harus login dulu');
	redirect(base_url('login'),'refresh');
}
require_once('head.php');
require_once('header.php');
require_once('nav.php');
require_once('konten.php');
require_once('footer.php');