   <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                    <li>
                        <a  href="<?= base_url('admin/dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a>
                    </li>

                   <li><a href="#"><i class="fa fa-user"></i>Admin <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url('admin/user') ?>">Data Admin</a></li>
                            <li><a href="<?php echo base_url('admin/user/tambah') ?>">Tambah Admin</a></li>
                        </ul>                       
                   </li>

                   <!-- // menu anggota -->
                   <li><a href="<?php echo base_url('admin/anggota') ?>"><i class="fa fa-group"></i>Angota</a>                      
                   </li>

                   <!-- menu buku -->
                   <li><a href="#"><i class="fa fa-book"></i>Buku<span class="fa arrow"></span></a> 
                   <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('admin/buku') ?>">Data Buku</a></li>
                    <li><a href="<?php echo base_url('admin/file_buku') ?>">Kelola File Buku</a></li>
                   </ul>                     
                   </li>

                   <!-- menu peminjaman buku -->
                   <li><a href="#"><i class="fa fa-calendar"></i>Peminjaman Buku<span class="fa arrow"></span></a> 
                   <ul class="nav nav-second-level">
                    <li><a href="<?php echo base_url('admin/peminjaman') ?>">Data Peminjaman Buku</a></li>
                    <li><a href="<?php echo base_url('admin/peminjaman/tambah') ?>">Tambah Peminjaman Buku</a></li>
                   </ul>                     
                   </li>

                   <!-- menu berita -->
                   <li><a href="<?php echo base_url('admin/berita') ?>"><i class="fa fa-eye"></i>Berita</a>                      
                   </li>

                   
                   <!-- menu referensi -->
                   <li><a href="#"><i class="fa fa-tags"></i>Referensi <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url('admin/jenis') ?>">Jenis Buku</a></li>
                            <li><a href="<?php echo base_url('admin/bahasa') ?>">Bahasa Buku</a></li>
                            <li><a href="<?php echo base_url('admin/link') ?>"> Data Link</a></li>

                        </ul>                       
                   </li>

                   <!-- menu referensi -->
                   <li><a href="#"><i class="fa fa-gear"></i>Konfiggurasi <span class="fa arrow"></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url('admin/konfigurasi') ?>">Konfigurasi Umum</a></li>
                            <li><a href="<?php echo base_url('admin/konfigurasi/logo') ?>">Konfigurasi Logo</a></li>
                            <li><a href="<?php echo base_url('admin/konfigurasi/icon') ?>">Konfigurasi Ikon</a></li>
                            <li> <a href="<?php echo base_url('admin/denda')?>">Denda</a></li>

                        </ul>                       
                   </li>


                   <li><a href="#"><i class="fa fa-file"></i>Laporan <span class="fa arrow"></span></a>
                         <ul class="nav nav-second-level">
                            <li><a href="<?php echo base_url('admin/laporan/laporan_anggota_aktif') ?>">Laporan Anggota Aktif</a></li>
                            <li><a href="<?php echo base_url('admin/laporan/laporan_data_peminjaman_buku') ?>">Laporan Status Peminjaman</a></li>
                            <li><a href="<?php echo base_url('admin/laporan/laporan_data_trend_buku') ?>">Laporan Tren Buku yang dipinjam</a></li>
                        </ul>      
                   </li>
                </ul>
               
            </div>
            
        </nav>  
        <!-- /. NAV SIDE  -->
        <div id="page-wrapper" >
            <div id="page-inner">
                <div class="row">
                    <div class="col-md-12">
                     <h2><?= $title ?></h2>                          
                    </div>
                </div>
                 <!-- /. ROW  -->
                 <hr />
               
            <div class="row">
                <div class="col-md-12">
                    <!-- Advanced Tables -->
                    <div class="panel panel-default">
                        <div class="panel-heading">
                             <?= $title ?>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">