<?php 
if ($this->uri->segment(3) != "") {
    include('tambah.php');
}else{
?>   
<p><a href="<?php echo base_url('admin/buku') ?>" class ="btn btn-success">
	<i class="fa fa-plus"></i>Tambah File</a></p>

<?php } ?>

<?php
//notif
if($this->session->flashdata('sukses')){
	echo '<div class="alert alert-success"><i class="fa fa-check"></i>';
	echo $this->session->flashdata('sukses');
	echo '</div>';
}
?>

<table class="table table-striped table-bordered table-hover" id="dataTables-example">
    <thead>
        <tr>
            <th>#</th>
            <th>Judul Buku</th>
            <th>Nama File</th>
            <th>Keterangan</th>
            <th>Urutan</th>
            <th width="23%">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php  $i = 1; foreach($file_buku as $file_buku) {   ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $file_buku->judul_file?>
                <br>
                <small>Judul Buku : <?php echo $file_buku->judul_buku ?></small>
            </td>
            <td><?php echo $file_buku->nama_file?></td>
            <td><?php echo $file_buku->keterangan?></td>
            <td><?php echo $file_buku->urutan?></td>
            <td>
            	<a href="<?php echo base_url('admin/file_buku/edit/' . $file_buku->id_file_buku) ?>" class = "btn btn-warning btn-xs"><i class="fa fa-pencil"></i>  Edit</a>

                <a href="<?php echo base_url('admin/file_buku/download/' . $file_buku->id_file_buku) ?>" class = "btn btn-info btn-xs"><i class="fa fa-download" target="_blank"></i>  Download</a>
            	
                <?php include('hapus.php'); ?>

            </td>
        </tr>
    <?php $i++; } ?>
    </tbody>
</table>