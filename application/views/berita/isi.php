<!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
            <div class="panel-body">
              <div class="row">
                <div class="col-md-4">
                  <img src="<?php echo base_url('assets/upload/image/thumbs/'.$berita->gambar)?>" class="img img-responsive img-thumbnail">
                </div>
            <h2><a href="<?php echo base_url('berita/read/'.$berita->slug_berita)?>"><?php echo $berita->judul_berita?></a></h2>
              <p><?php echo character_limiter($berita->isi,250) ?></p>
              <p><a href="<?php echo base_url('berita/read/'.$berita->slug_berita)?>">Baca Detail...</a></p>
          </div>
          </div>