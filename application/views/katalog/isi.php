      <!-- Example row of columns -->
      <div class="row">
        <div class="col-lg-8">
            

           <div class="panel panel-default">
            <div class="panel-body">


            <h2>Pencarian Buku</h2>

            <p class="alert alert-success">
                <i class="glyphicon glyphicon-warning-sign"></i> Ketik Judul Buku
              </p>
              <form action="<?php echo base_url('katalog')?>" method="post" class="form-inline text-center">
                <input type="text" name="cari" class="form-control" required placeholder="Kata Kunci">
                <input type="submit" name="submit" class="btn btn-primary" value="Cari">
              </form>
          </div>
          </div>

           
        
        </div>
    

        <div class="col-lg-4">
          <div class="panel panel-default">
          <div class="panel-body">
          <h2>Buku Terbaru</h2>
          <?php $a=1; foreach($buku as $buku){ ?>
          <!-- buku <?php echo $a?>-->
          <div class="row buku">

            <div class="col-md-4">
              <a href="<?php echo base_url('katalog/read/'.$buku->id_buku)?>">
              <img class="img img-thumbnail img-rounded" src="<?php if($buku->cover_buku=="") { echo base_url('assets/perpus/image/gambar.png'); } else{ echo base_url('assets/upload/image/thumbs/'.$buku->cover_buku);}?>" alt="<?php echo $buku->judul_buku ?>">
            </div>

            <div class="col-md-8">
              <h3><a href="<?php echo base_url('katalog/read/'.$buku->id_buku)?>"><?php echo $buku->judul_buku ?></a></h3>
              <p><?php echo character_limiter($buku->ringkasan,100) ?></p>
            </div>
            <div class="clearfix"></div>
            <hr>
          </div>
          <!-- end buku <?php echo $a?> -->
          <?php $a++; }?>
          <p>
          <a href="<?php echo base_url('katalog')?>" class ="btn btn-primary btn-block">
            <i class= "glyphicon glyphicon-book"></i> Lihat Semua Koleksi</a>
          </p>

        </div>
        </div>
        </div>
      </div>