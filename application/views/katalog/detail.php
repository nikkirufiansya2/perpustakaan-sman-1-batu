<!-- Example row of columns -->
  <div class="row">
    <div class="col-lg-12">
        

       <div class="panel panel-default">
        <div class="panel-body">
        <h2><?php echo $title?></h2>

    <div class="row">
    	<p class="text-right">
    		<a href="<?php echo base_url('katalog')?>" class ="btn btn-group-xs">
    		<i class ="fa fa-backward"></i> Kembali dan cari buku lain
    	</a>
    	</p>
    	<hr>


        <div class="col-md-4">
        	<?php if($buku->cover_buku!=""){ ?>
        		<img src="<?php echo base_url('assets/upload/image/'.$buku->cover_buku)?>" class="img img-thumbnail img-responsive">

        	<?php }else { echo 'Tidak Ada Cover';} ?>

        </div>
        <div class="col-md-8">

        	<?php if(count($file_buku) <1) {  ?>
        		<p class ="alert alert-success text-center">
        			<i class="glyphicon glyphicon-warning-sign"></i> File Buku tidak tersedia
        		</p>
        	<?php }else{ ?>
        		<table class="table table-striped table-bordered table-hover">
    <thead>
        <tr>
            <th width="5%">No</th>
            <th>Judul File</th>
           <!--  <th>Keterangan</th> -->
            <th width="23%">Aksi</th>
        </tr>
    </thead>
    <tbody>
    <?php  $i = 1; foreach($file_buku as $file_buku) {   ?>
        <tr>
            <td><?php echo $i ?></td>
            <td><?php echo $file_buku->judul_file?></td>
            <!-- <td><?php echo $file_buku->keterangan?></td> -->
            <td>
            	<a href="<?php echo base_url('katalog/baca/'.$file_buku->id_file_buku)?>" class = "btn btn-success btn-xs"><i class="fa fa-eye"></i>  Lihat PDF</a>
            </td>
        </tr>
    <?php $i++; } ?>
    </tbody>
</table>
        	<?php } ?>

        	<table class="table table-bordered table-hover">
        		<thead>
        			<tr>
        				<th width="30%">Judul Buku</th>
        				<th>: <?php echo $buku->judul_buku ?></th>
        			</tr>
        		</thead>
        		<tbody>
        			<tr>
        				<td>Penulis</td>
        				<td>: <?php echo$buku->penulis_buku ?></td>
        			</tr>
        			<tr>
        				<td>Subjek</td>
        				<td>: <?php echo$buku->subjek_buku ?></td>
        			</tr>
        			<tr>
        				<td>Letak</td>
        				<td>: <?php echo$buku->letak_buku ?></td>
        			</tr>
        			<tr>
        				<td>Penerbit</td>
        				<td>: <?php echo$buku->penerbit ?></td>
        			</tr>
        			<tr>
        				<td>ISBN</td>
        				<td>: <?php echo$buku->nomor_seri ?></td>
        			</tr>
        			<tr>
        				<td>Berat</td>
        				<td>: <?php echo$buku->kolasi ?></td>
        			</tr>
        			<tr>
        				<td>Tahun Terbit</td>
        				<td>: <?php echo$buku->tahun_terbit ?></td>
        			</tr>
        			<tr>
        				<td>Deskripsi</td>
        				<td>: <?php echo$buku->ringkasan ?></td>
        			</tr>
        		</tbody>
        	</table>

    </div>

    </div>
</div>
</div>
</div>