 <!-- Static navbar -->
 <?php
//memndapatkan id user yg login
 $id_user    = $this->session->userdata('id_user');
 $grade    = $this->session->userdata('akses_level');
 $user_aktif = $this->user_model->detail($id_user); 
 ?>

 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <?php if(!empty($id_user)){ ?>
          <li class=""><a href="<?php echo base_url('/home/homeUser') ?>">Home</a></li>
        <?php }else{ ?>
          <li class=""><a href="<?php echo base_url() ?>">Home</a></li>
        <?php }?>
        <li><a href="<?php echo base_url('katalog') ?>">Katalog Buku</a></li>
        <li><a href="<?php echo base_url('kontak') ?>">Kontak</a></li>
        <li><a href="<?php echo base_url('data_peminjaman_anggota')?>">Data Peminjaman</a></li>
        <?php if(!empty($id_user)){ ?>
          <li><a href="<?php echo base_url('login/logout') ?>"><i class="fa fa-login"></i>Logout</a></li>
         
          <?php }else{ ?>
            <li><a href="<?php echo base_url('login') ?>"><i class="fa fa-login"></i>Masuk</a></li> 
          <?php }?>
        </ul>
      </div><!--/.nav-collapse -->
    </div><!--/.container-fluid -->
  </nav>
</div>