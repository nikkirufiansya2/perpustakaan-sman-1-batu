<?php 
//load data konfigurasi
$konfigurasi = $this->konfigurasi_model->listing();
?>

 <!-- Site footer -->
      <footer class="footer">
        <p>&copy; <?php echo date('Y') ?> . <?php echo $konfigurasi->namaweb ?> | Email : <?php echo $konfigurasi->email ?></p>
      </footer>

    </div> <!-- /container -->

    <script type="text/javascript" src="<?php echo base_url('assets/perpus/js/bootstrap.min.js')?>"></script>
      <!-- DATA TABLE SCRIPTS -->
    <script src="<?= base_url ('assets/assets/binary/')?>assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="<?= base_url ('assets/assets/binary/')?>assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
  </body>
</html>
