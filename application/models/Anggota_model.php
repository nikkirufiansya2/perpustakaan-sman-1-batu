<?php

class anggota_model extends CI_model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	//Login admin
	public function login ($anggotaname,$password){
		$this->db->select('*');
		$this->db->from('anggota');
		$this->db->where(array('anggotaname' => $anggotaname,
								'password'=>sha1($password)));
		$this->db->order_by('id_anggota','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//Login anggota
	public function loginAnggota ($anggotaname,$password){
		$this->db->select('*');
		$this->db->from('anggota'); 
		$this->db->where(array('anggotaname' => $anggotaname,
								'password'=>sha1($password)));
		$this->db->order_by('id_anggota','DESC');
		$query = $this->db->get();
		return $query->row();
	}
	//tambah data anggota
	public function tambah ($data){
		$this->db->insert('anggota',$data);
	}

	//daftar anggota
	public function listing(){
		$this->db->select('*');
		$this->db->from('anggota');
		$this->db->order_by('id_anggota', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_anggota){
		$this->db->select('*');
		$this->db->from('anggota');
		$this->db->where('id_anggota',$id_anggota);
		$this->db->order_by('id_anggota', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//edit data anggota
	public function edit($data){
		$this->db->where('id_anggota',$data['id_anggota']);
		$this->db->update('anggota',$data);
	}

	//hapus data
	public function hapus($data){
		$this->db->where('id_anggota',$data['id_anggota']);
		$this->db->delete('anggota',$data);
	}

}
