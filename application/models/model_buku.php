<?php

/**
 * 
 */
class model_buku extends CI_model
{
	
	public function getAll()
	{
		$query = $this->db->select("*")
				 ->from('buku')
				 ->order_by('id_buku', 'DESC')
				 ->get();
		return $query->result();
	}

	public function simpan($data)
	{
		
		$query = $this->db->insert("buku", $data);

		if($query){
			return true;
		}else{
			return false;
		}

	}

	public function edit($id_buku)
	{
		
		$query = $this->db->where("id_buku", $id_buku)
				->get("buku");

		if($query){
			return $query->row();
		}else{
			return false;
		}

	}

	public function update($data, $id)
	{
		
		$query = $this->db->update("buku", $data, $id);

		if($query){
			return true;
		}else{
			return false;
		}

	}

	public function detail ($id_buku)
	{
		$query = $this->db->where("id_buku", $id_buku)
				->get("buku");

		if($query){
			return $query->row();
		}else{
			return false;
		}
	}

	public function hapus($id)
	{
		
		$query = $this->db->delete("buku", $id);

		if($query){
			return true;
		}else{
			return false;
		}

	}
}