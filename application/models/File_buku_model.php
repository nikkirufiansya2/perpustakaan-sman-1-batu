<?php

class File_buku_model extends CI_model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	//tambah data file_buku
	public function tambah ($data){
		$this->db->insert('file_buku',$data);
	}

	//daftar file_buku
	public function listing(){
		$this->db->select('file_buku.*,
							buku.judul_buku,
							user.nama');
		$this->db->from('file_buku');
		//join tabel
		$this->db->join('buku','buku.id_buku = file_buku.id_buku', 'LEFT');
		$this->db->join('user','user.id_user = file_buku.id_user', 'LEFT');
		//
		$this->db->order_by('urutan', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	//daftar file perbuku
	public function buku($id_buku){
		$this->db->select('file_buku.*,
							buku.judul_buku,
							user.nama');
		$this->db->from('file_buku');
		//join tabel
		$this->db->join('buku','buku.id_buku = file_buku.id_buku', 'LEFT');
		$this->db->join('user','user.id_user = file_buku.id_user', 'LEFT');
		//akhir join 
		$this->db->where('file_buku.id_buku',$id_buku);
		$this->db->order_by('urutan', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}


	//detail
	public function detail($id_file_buku){
		$this->db->select('*');
		$this->db->from('file_buku');
		$this->db->where('id_file_buku',$id_file_buku);
		$this->db->order_by('id_file_buku', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//edit data file_buku
	public function edit($data){
		$this->db->where('id_file_buku',$data['id_file_buku']);
		$this->db->update('file_buku',$data);
	}

	//hapus data
	public function hapus($data){
		$this->db->where('id_file_buku',$data['id_file_buku']);
		$this->db->delete('file_buku',$data);
	}

}
