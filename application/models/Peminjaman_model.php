<?php

class Peminjaman_model extends CI_model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	
	//tambah data peminjaman
	public function tambah ($data){
		$this->db->insert('peminjaman',$data);
	}

	//daftar peminjaman
	public function listing(){
		$this->db->select('peminjaman.*,
							buku.judul_buku,
							buku.kode_buku,
							buku.nomor_seri,
							buku.penerbit,
							anggota.nama_anggota');
		$this->db->from('peminjaman');
		//join
		$this->db->join('anggota','anggota.id_anggota = peminjaman.id_anggota'); 
		$this->db->join('buku','buku.id_buku = peminjaman.id_buku');
		//End join 
		$this->db->order_by('id_peminjaman', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	//daftar peminjaman anggota
	public function anggota($id_anggota){
		$this->db->select('peminjaman.*,
							buku.judul_buku,
							buku.kode_buku,
							buku.nomor_seri,
							buku.penerbit,
							anggota.nama_anggota');
		$this->db->from('peminjaman');
		//join
		$this->db->join('anggota','anggota.id_anggota = peminjaman.id_anggota'); 
		$this->db->join('buku','buku.id_buku = peminjaman.id_buku');
		//End join 
		$this->db->where('peminjaman.id_anggota',$id_anggota);
		$this->db->order_by('id_peminjaman', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	//limit peminjaman anggota
	public function limit_peminjaman_anggota($id_anggota){
		$this->db->select('peminjaman.*,
							buku.judul_buku,
							buku.kode_buku,
							buku.nomor_seri,
							buku.penerbit,
							anggota.nama_anggota');
		$this->db->from('peminjaman');
		//join
		$this->db->join('anggota','anggota.id_anggota = peminjaman.id_anggota'); 
		$this->db->join('buku','buku.id_buku = peminjaman.id_buku');
		//End join 
		$this->db->where('peminjaman.id_anggota',$id_anggota);
		$this->db->where('peminjaman.status_kembali <>','sudah');
		$this->db->order_by('id_peminjaman', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_peminjaman){
		$this->db->select('*');
		$this->db->from('peminjaman');
		$this->db->where('id_peminjaman',$id_peminjaman);
		$this->db->order_by('id_peminjaman', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//edit data peminjaman
	public function edit($data){
		$this->db->where('id_peminjaman',$data['id_peminjaman']);
		$this->db->update('peminjaman',$data);
	}

	//hapus data
	public function hapus($data){
		$this->db->where('id_peminjaman',$data['id_peminjaman']);
		$this->db->delete('peminjaman',$data);
	}

	public function peminjamanByUser($id_anggota)
	{
		$this->db->select('peminjaman.*,
							buku.judul_buku,
							buku.kode_buku,
							buku.nomor_seri,
							buku.penerbit,
							anggota.nama_anggota');
		$this->db->from('peminjaman');
		//join
		$this->db->join('anggota','anggota.id_anggota = peminjaman.id_anggota'); 
		$this->db->join('buku','buku.id_buku = peminjaman.id_buku');
		//End join 
		$this->db->order_by('id_peminjaman', 'DESC');
		$this->db->where('anggota.id_anggota', $id_anggota);
		$query = $this->db->get();
		return $query->result();
	}

}
