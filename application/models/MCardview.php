<?php 
class mcardview extends CI_model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	public function anggota_aktif()
	{   

		$this->db->select('count(id_user) as jumlah');
		$this->db->where('status_anggota','aktif');
		$this->db->from('anggota');
		$query = $this->db->get();
		return $query->result();
	}

	public function jumlah_buku()
	{   

		$this->db->select('count(id_buku) as jbuku');
		$this->db->from('buku');
		$query = $this->db->get();
		return $query->result();
	}

	public function PHini()
	{   

		$this->db->select('count(id_peminjaman) as PHini');
		$this->db->from('peminjaman');
		$this->db->where('tanggal_pinjam','NOW()');
		$query = $this->db->get();
		return $query->result();
	}

	public function PHKini()
	{   

		$this->db->select('count(id_peminjaman) as PHKini');
		$this->db->from('peminjaman');
		$this->db->where('tanggal_kembali','NOW()');
		$query = $this->db->get();
		return $query->result();
	}


}

?>