<?php

class Link_model extends CI_model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	//tambah data link
	public function tambah ($data){
		$this->db->insert('link',$data);
	}

	//daftar link
	public function listing(){
		$this->db->select('*');
		$this->db->from('link');
		$this->db->order_by('id_link', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_link){
		$this->db->select('*');
		$this->db->from('link');
		$this->db->where('id_link',$id_link);
		$this->db->order_by('id_link', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//edit data link
	public function edit($data){
		$this->db->where('id_link',$data['id_link']);
		$this->db->update('link',$data);
	}

	//hapus data
	public function hapus($data){
		$this->db->where('id_link',$data['id_link']);
		$this->db->delete('link',$data);
	}

}
