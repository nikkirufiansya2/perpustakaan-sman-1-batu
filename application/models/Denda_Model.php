<?php
	/**
	 * 
	 */
	class Denda_Model extends CI_Model
	{
		
		public function __construct(){
			parent::__construct();
			$this->load->database();
		}

		public function listing(){
			$this->db->select('*');
			$this->db->from('denda');
			$query = $this->db->get();
			return $query->result();
		}

		public function getId($id)
		{
			$this->db->select('*');
			$this->db->from('denda');
			$this->db->where('id', $id);
			$query = $this->db->get();
			return $query->row();
		}

		public function edit($data){
		$this->db->where('id',$data['id']);
		$this->db->update('denda',$data);
	}

	}

	?>