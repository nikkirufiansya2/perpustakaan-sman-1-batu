<?php

class Bahasa_model extends CI_model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}
	
	//tambah data bahasa
	public function tambah ($data){
		$this->db->insert('bahasa',$data);
	}

	//daftar bahasa
	public function listing(){
		$this->db->select('*'); 
		$this->db->from('bahasa');
		$this->db->order_by('urutan', 'ASC');
		$query = $this->db->get();
		return $query->result();
	}

	//detail
	public function detail($id_bahasa){
		$this->db->select('*');
		$this->db->from('bahasa');
		$this->db->where('id_bahasa',$id_bahasa);
		$this->db->order_by('id_bahasa', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//edit data bahasa
	public function edit($data){
		$this->db->where('id_bahasa',$data['id_bahasa']);
		$this->db->update('bahasa',$data);
	}

	//hapus data
	public function hapus($data){
		$this->db->where('id_bahasa',$data['id_bahasa']);
		$this->db->delete('bahasa',$data);
	}

}
