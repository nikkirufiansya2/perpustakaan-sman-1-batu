<?php

class Buku_model extends CI_model {

	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	//Login admin
	public function login ($bukuname,$password){
		$this->db->select('*');
		$this->db->from('buku');
		$this->db->where(array('bukuname' => $bukuname,
								'password'=>sha1($password)));
		$this->db->order_by('id_buku','DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//Login buku
	public function loginAnggota ($bukuname,$password){
		$this->db->select('*');
		$this->db->from('anggota');
		$this->db->where(array('bukuname' => $bukuname,
								'password'=>sha1($password)));
		$this->db->order_by('id_buku','DESC');
		$query = $this->db->get();
		return $query->row();
	}
	//tambah data buku
	public function tambah ($data){
		$this->db->insert('buku',$data);
	}

	//daftar buku
	public function listing(){
		$this->db->select('buku.*,
						  	jenis.nama_jenis,
						  	jenis.kode_jenis,
						  	bahasa.nama_bahasa,
						  	bahasa.kode_bahasa,
						  	user.nama');
		$this->db->from('buku');
		//join 4 table
		$this->db->join('jenis','jenis.id_jenis = buku.id_jenis','LEFT');
		$this->db->join('bahasa','bahasa.id_bahasa = buku.id_bahasa','LEFT');
		$this->db->join('user','user.id_user = buku.id_user','LEFT');

		//end join
		$this->db->order_by('id_buku', 'DESC');
		$query = $this->db->get();
		return $query->result();
	}

	//Buku 
	public function buku(){
		$this->db->select('buku.*,
						  	jenis.nama_jenis,
						  	jenis.kode_jenis,
						  	bahasa.nama_bahasa,
						  	bahasa.kode_bahasa,
						  	user.nama');
		$this->db->from('buku');
		//join 4 table
		$this->db->join('jenis','jenis.id_jenis = buku.id_jenis','LEFT');
		$this->db->join('bahasa','bahasa.id_bahasa = buku.id_bahasa','LEFT');
		$this->db->join('user','user.id_user = buku.id_user','LEFT');

		//end join
		$this->db->where('buku.status_buku','Publish');
		$this->db->order_by('id_buku', 'DESC');
		$this->db->limit(5);
		$query = $this->db->get();
		return $query->result();
	}

	//Buku Terbaru
	public function baru(){
		$this->db->select('buku.*,
						  	jenis.nama_jenis,
						  	jenis.kode_jenis,
						  	bahasa.nama_bahasa,
						  	bahasa.kode_bahasa,
						  	user.nama');
		$this->db->from('buku');
		//join 4 table
		$this->db->join('jenis','jenis.id_jenis = buku.id_jenis','LEFT');
		$this->db->join('bahasa','bahasa.id_bahasa = buku.id_bahasa','LEFT');
		$this->db->join('user','user.id_user = buku.id_user','LEFT');

		//end join
		$this->db->where('buku.status_buku','Publish');
		$this->db->order_by('id_buku', 'DESC');
		$this->db->limit(20);
		$query = $this->db->get();
		return $query->result();
	}


	//Cari buku
	public function cari($keywords){
		$this->db->select('buku.*,
						  	jenis.nama_jenis,
						  	jenis.kode_jenis,
						  	bahasa.nama_bahasa,
						  	bahasa.kode_bahasa,
						  	user.nama');
		$this->db->from('buku');
		//join 4 table
		$this->db->join('jenis','jenis.id_jenis = buku.id_jenis','LEFT');
		$this->db->join('bahasa','bahasa.id_bahasa = buku.id_bahasa','LEFT');
		$this->db->join('user','user.id_user = buku.id_user','LEFT');

		//end join
		$this->db->where('buku.status_buku','Publish');
		$this->db->like('buku.judul_buku',$keywords);
		$this->db->order_by('id_buku', 'DESC');
		$this->db->limit(5);
		$query = $this->db->get();
		return $query->result();
	}

	//read buku
	public function read($id_buku){
		$this->db->select('buku.*,
						  	jenis.nama_jenis,
						  	jenis.kode_jenis,
						  	bahasa.nama_bahasa,
						  	bahasa.kode_bahasa,
						  	user.nama');
		$this->db->from('buku');
		//join 4 table
		$this->db->join('jenis','jenis.id_jenis = buku.id_jenis','LEFT');
		$this->db->join('bahasa','bahasa.id_bahasa = buku.id_bahasa','LEFT');
		$this->db->join('user','user.id_user = buku.id_user','LEFT');

		//end join
		$this->db->where('buku.status_buku','Publish');
		$this->db->where('id_buku',$id_buku);
		$this->db->order_by('id_buku', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}


	//detail
	public function detail($id_buku){
		$this->db->select('*');
		$this->db->from('buku');
		$this->db->where('id_buku',$id_buku);
		$this->db->order_by('id_buku', 'DESC');
		$query = $this->db->get();
		return $query->row();
	}

	//edit data buku
	public function edit($data){
		$this->db->where('id_buku',$data['id_buku']);
		$this->db->update('buku',$data);
	}

	//hapus data
	public function hapus($data){
		$this->db->where('id_buku',$data['id_buku']);
		$this->db->delete('buku',$data);
	}

}
