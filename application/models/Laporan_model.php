<?php
/**
 * 
 */
class Laporan_Model extends CI_Model
{
	
	public function __construct(){
		parent::__construct();
		$this->load->database();
	}

	public function listingAnggotaAktif()
	{
		$this->db->select('*');
		$this->db->from('anggota');
		$this->db->where('status_anggota','aktif');
		$query = $this->db->get();
		return $query->result();
	}

	public function trend_buku()
	{
		$query = $this->db->query("SELECT COUNT(peminjaman.id_buku) as total_peminjaman ,buku.judul_buku, buku.penulis_buku, buku.penerbit FROM buku,peminjaman WHERE buku.id_buku = peminjaman.id_buku GROUP BY buku.id_buku DESC");
		return $query->result();
	}
}


?>