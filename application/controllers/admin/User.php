<?php
Class User extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('user_model');
	}
	
	//halaman utama user
	public function index (){
		$user = $this->user_model->listing();

		$data = array ('title' => 'Data Admin ('.count($user).')',
						'user' => $user,
					   'konten' => 'sadmin/user/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
	}

	//halaman tambah user
	public function tambah (){

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('username','Username','required|is_unique[user.username]',
				array('required' => 'Username harus di isi',
					  'is_unique' => 'Username sudah di gunakan'));

		$valid->set_rules('password','Password','required|min_length[8]',
				array('required' => 'password harus di isi',
					  'min_length' => 'Password minimal 8 karakter'));

		$valid->set_rules('email','Email','required|valid_email',
				array('required' => 'email harus di isi',
					  'valid_email' => 'Format email anda salah'));

		$valid->set_rules('nama','Nama','required',
				array('required' => 'nama harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Tambah User',
					   'konten' => 'sadmin/user/tambah');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;
			$data = array ( 'nama'				=> $i->post('nama'),
							'email'				=> $i->post('email'),
							'username'			=> $i->post('username'),
							'password'			=> sha1($i->post('password')),
							'akses_level'		=> $i->post('akses_level'),
							'keterangan'		=> $i->post('keterangan')
			);
			$this->user_model->tambah($data);
			$this->session->set_flashdata('sukses','Data berhasil ditambah');
			redirect(base_url('admin/user'),'refresh');
		}
	}

	//halaman edit user
	public function edit ($id_user){
		$user = $this->user_model->detail($id_user); 

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('email','Email','required|valid_email',
				array('required' => 'email harus di isi',
					  'valid_email' => 'Format email anda salah'));

		$valid->set_rules('nama','Nama','required',
				array('required' => 'nama harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Edit User '.$user ->nama,
						'user' =>$user,
					   'konten' => 'sadmin/user/edit');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;

			//password di ganti jika diisi lebih dari 8 karakter
			if(strlen($i->post('password'))>8){

				$data = array ( 'id_user'			=> $id_user,
								'nama'				=> $i->post('nama'),
								'email'				=> $i->post('email'),
								'password'			=> sha1($i->post('password')),
								'akses_level'		=> $i->post('akses_level'),
								'keterangan'		=> $i->post('keterangan')
				);

			} else {

				$data = array ( 'id_user'			=> $id_user,
								'nama'				=> $i->post('nama'),
								'email'				=> $i->post('email'),
								'akses_level'		=> $i->post('akses_level'),
								'keterangan'		=> $i->post('keterangan')
				);
			}

			$this->user_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil diupdate');
			redirect(base_url('admin/user'),'refresh');
		}
	}

	//hapus data user
	public function hapus($id_user){
		//perlindungan
		if ($this->session->userdata('username')== "" && $this->session->userdata('akses_level')=="") {
			$this->session->set_flashdata('sukses','Anda Harus login dulu');
			redirect(base_url('login'),'refresh');
		}
		
		$data = array('id_user' => $id_user);
		$this->user_model->hapus($data);
			$this->session->set_flashdata('sukses','Data berhasil dihapus');
			redirect(base_url('admin/user'),'refresh');
	}

}