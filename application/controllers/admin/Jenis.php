<?php
Class Jenis extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('jenis_model');
	}

	//halaman utama
	public function index (){
		$jenis = $this->jenis_model->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('kode_jenis','Kode Jenis Buku','required|is_unique[jenis.kode_jenis]',
				array('required' => 'Kode Jenis Buku harus di isi',
					  'is_unique' => 'Kode Jenis Buku sudah di gunakan'));

		$valid->set_rules('nama_jenis','Nama_jenis','required',
				array('required' => 'Nama jenis harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Kelola Jenis Buku',
						'jenis' => $jenis,
					   'konten' => 'sadmin/jenis/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;
			$data = array ( 'kode_jenis'		=> $i->post('kode_jenis'),
							'nama_jenis'		=> $i->post('nama_jenis'),							
							'keterangan'		=> $i->post('keterangan'),
							'urutan'			=> $i->post('urutan')
			);
			$this->jenis_model->tambah($data);
			$this->session->set_flashdata('sukses','Data berhasil ditambah');
			redirect(base_url('admin/jenis'),'refresh');
		}
	}

	//halaman edit jenis
	public function edit ($id_jenis){
		$jenis = $this->jenis_model->detail($id_jenis); 

		//validasi
		$valid = $this->form_validation;


		$valid->set_rules('nama_jenis','Nama_jenis','required',
				array('required' => 'Nama jenis harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Edit Jenis '.$jenis ->nama_jenis,
						'jenis' =>$jenis,
					   'konten' => 'sadmin/jenis/edit');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;

				$data = array ( 'id_jenis'			=> $id_jenis,
								'kode_jenis'		=> $i->post('kode_jenis'),
								'nama_jenis'		=> $i->post('nama_jenis'),						
								'keterangan'		=> $i->post('keterangan'),
								'urutan'			=> $i->post('urutan')
				);

			$this->jenis_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil diupdate');
			redirect(base_url('admin/jenis'),'refresh');
		}
	}

	//hapus data jenis
	public function hapus($id_jenis){
		//perlindungan
		if ($this->session->userdata('username')== "" && $this->session->userdata('akses_level')=="") {
			$this->session->set_flashdata('sukses','Anda Harus login dulu');
			redirect(base_url('login'),'refresh');
		}
		
		$data = array('id_jenis' => $id_jenis);
		$this->jenis_model->hapus($data);
			$this->session->set_flashdata('sukses','Data berhasil dihapus');
			redirect(base_url('admin/jenis'),'refresh');
	}

} 