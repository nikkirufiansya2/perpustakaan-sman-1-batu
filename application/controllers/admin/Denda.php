<?php
	/**
	 * 
	 */
	class Denda extends CI_Controller
	{
		
		public function __construct(){
			parent::__construct();
			$this->load->model('Denda_Model');
		}
		
		public function index()
		{
			$denda = $this->Denda_Model->listing();

			$data = array('title' => 'Data Anggota ('.count($denda).')',
				'denda' => $denda,
				'konten' => 'sadmin/denda/isi');
			$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}

		public function edit($id)
		{
			$denda = $this->Denda_Model->getId($id);
			$valid = $this->form_validation;
			$valid->set_rules('denda','Denda','required',
				array('required' => 'Total denda harus di isi'));

			if ($valid->run()=== FALSE) {
				
				$data = array('title' => 'Data Anggota ('.count($denda).')',
					'denda' => $denda,
					'konten' => 'sadmin/denda/edit');
				$this->load->view('sadmin/layout/wrapper',$data, FALSE);
			}else{
				$i = $this->input;

				$data = array( 'id' => $id,
					'denda' => $i->post('denda'));

				$this->Denda_Model->edit($data);
				$this->session->set_flashdata('sukses','Total Denda berhasil diupdate');
				redirect(base_url('admin/denda'),'refresh');
			}
			$data = array ('title'=> 'Edit Denda : '.$denda->denda,
				'denda' 	=> $denda,
				'konten' 	=> 'sadmin/berita/edit');
			$this->load->view('sadmin/layout/wrapper',$data, FALSE);

		}
	}

	?>