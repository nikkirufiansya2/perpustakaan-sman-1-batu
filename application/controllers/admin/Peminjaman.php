<?php

/**
 * 
 */
class Peminjaman extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('peminjaman_model');
		$this->load->model('buku_model');
		$this->load->model('anggota_model');
		$this->load->model('Denda_Model');
	}
	//halaman peminjaman
	public function index() {
		$peminjaman = $this->peminjaman_model->listing();
		$denda = $this->Denda_Model->listing();
		$data = array ('title'		=> 'Data Peminjaman ('.count($peminjaman).')',
						'peminjaman'=> $peminjaman,
						'denda' => $denda,
						'konten' => 'sadmin/peminjaman/isi'
						);
		$this->load->view('sadmin/layout/wrapper',$data,FALSE); 
	}
	//tambah peminjaman
	public function tambah() {
		$anggota = $this->anggota_model->listing();

		$data = array ('title'		=> 'Tambah Data Peminjaman',
						'anggota'=> $anggota,
						'konten' => 'sadmin/peminjaman/list_anggota'
						);
		$this->load->view('sadmin/layout/wrapper',$data,FALSE); 
	}
	//add peminjaman
	public function add ($id_anggota){
		$anggota 		= $this->anggota_model->detail($id_anggota);
		$peminjaman 	= $this->peminjaman_model->anggota($id_anggota);
		$limit			= $this->peminjaman_model->limit_peminjaman_anggota($id_anggota);
		$buku 			= $this->buku_model->listing();
		$konfigurasi	= $this->konfigurasi_model->listing();

		//validasi
		$valid = $this->form_validation;	

		$valid->set_rules('id_buku', 'Pilih Buku','required',
					array(	'required'	=> 'Pilih Judul Buku'));

		if($valid->run()=== FALSE) {
			//end  validasi

		$data = array ('title'		=> 'Peminjaman Buku :'.$anggota->nama_anggota,
						'anggota'	=> $anggota,
						'peminjaman'=> $peminjaman,
						'buku'		=> $buku,
						'konfigurasi'=> $konfigurasi,
						'limit'		=> $limit,
						'konten' 	=> 'sadmin/peminjaman/tambah'
						);
		$this->load->view('sadmin/layout/wrapper',$data,FALSE); 	
		//masuk database
		} else {
			$i = $this->input;
			$data = array ('id_user' 		=> $this->session->userdata('id_user'),
							'id_buku'		=> $i->post('id_buku'),
							'id_anggota'	=> $id_anggota,
							'tanggal_pinjam'=> $i->post('tanggal_pinjam'),
							'tanggal_kembali'=> $i->post('tanggal_kembali'),
							'keterangan'	=> $i->post('keterangan'),
							'status_kembali'=> $i->post('status_kembali')
						);
			$this->peminjaman_model->tambah($data);
			$this->session->set_flashdata('sukses','Data berhasil di tambahkan');
			redirect(base_url('admin/peminjaman/add/'.$id_anggota),'refresh');
		}	
	}

	//edit peminjaman
	public function edit ($id_peminjaman){
		$peminjaman 	= $this->peminjaman_model->detail($id_peminjaman);
		$id_anggota		= $peminjaman->id_anggota;
		$anggota 		= $this->anggota_model->detail($id_anggota);
		$buku 			= $this->buku_model->listing();
		$konfigurasi	= $this->konfigurasi_model->listing();

		//validasi
		$valid = $this->form_validation;	

		$valid->set_rules('id_buku', 'Pilih Buku','required',
					array(	'required'	=> 'Pilih Judul Buku'));

		if($valid->run()=== FALSE) {
			//end  validasi

		$data = array ('title'		=> 'Edit Peminjaman Buku :'.$anggota->nama_anggota,
						'anggota'	=> $anggota,
						'peminjaman'=> $peminjaman,
						'buku'		=> $buku,
						'konfigurasi'=> $konfigurasi,
						'konten' 	=> 'sadmin/peminjaman/edit'
						);
		$this->load->view('sadmin/layout/wrapper',$data,FALSE); 	
		//masuk database
		} else {
			$i = $this->input;
			$data = array (	'id_peminjaman'	=> $id_peminjaman,
							'id_user' 		=> $this->session->userdata('id_user'),
							'id_buku'		=> $i->post('id_buku'),
							'id_anggota'	=> $id_anggota,
							'tanggal_pinjam'=> $i->post('tanggal_pinjam'),
							'tanggal_kembali'=> $i->post('tanggal_kembali'),
							'keterangan'	=> $i->post('keterangan'),
							'status_kembali'=> $i->post('status_kembali')
						);
			$this->peminjaman_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil di update');
			redirect(base_url('admin/peminjaman'),'refresh');
		}	
	}

	//pengembalian buku
	public function kembali ($id_peminjaman){
		$peminjaman 	= $this->peminjaman_model->detail($id_peminjaman);

		$data = array (	'id_peminjaman'	=> $id_peminjaman,
						'id_user' 		=> $this->session->userdata('id_user'),
						'status_kembali'=> 'sudah'
						);
			$this->peminjaman_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil di update');
			redirect(base_url('admin/peminjaman'),'refresh');

	}

	//hapus data peminjaman
	public function hapus($id_peminjaman){
		//perlindungan
		if ($this->session->userdata('username')== "" && $this->session->userdata('akses_level')=="") {
			$this->session->set_flashdata('sukses','Anda Harus login dulu');
			redirect(base_url('login'),'refresh');
		}
		
		$data = array('id_peminjaman' => $id_peminjaman);
		$this->peminjaman_model->hapus($data);
			$this->session->set_flashdata('sukses','Data berhasil dihapus');
			redirect(base_url('admin/peminjaman'),'refresh');
	}
}
?>