<?php
Class Link extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('link_model');
	}

	//halaman utama
	public function index (){
		$link = $this->link_model->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('url','Alamat Link','required|is_unique[link.url]',
				array('required' => 'Alamat Link harus di isi',
					  'is_unique' => 'Alamat Link sudah di gunakan'));

		$valid->set_rules('nama_link','Nama_link','required',
				array('required' => 'Nama link harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Kelola Link Perpustakaan lain',
						'link' => $link,
					   'konten' => 'sadmin/link/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;
			$data = array ( 'nama_link'		=> $i->post('nama_link'),
							'url'			=> $i->post('url'),							
							'target'		=> $i->post('target')
			);
			$this->link_model->tambah($data);
			$this->session->set_flashdata('sukses','Data berhasil ditambah');
			redirect(base_url('admin/link'),'refresh');
		}
	}

	//halaman edit link
	public function edit ($id_link){
		$link = $this->link_model->detail($id_link); 

		//validasi
		$valid = $this->form_validation;


		$valid->set_rules('nama_link','Nama_link','required',
				array('required' => 'Nama link harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Edit Link '.$link ->nama_link,
						'link' =>$link,
					   'konten' => 'sadmin/link/edit');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;

				$data = array ( 'id_link'		=> $id_link,
								'nama_link'		=> $i->post('nama_link'),
								'url'			=> $i->post('url'),							
								'target'		=> $i->post('target')
				);

			$this->link_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil diupdate');
			redirect(base_url('admin/link'),'refresh');
		}
	}

	//hapus data link
	public function hapus($id_link){
		//perlindungan
		if ($this->session->userdata('username')== "" && $this->session->userdata('akses_level')=="") {
			$this->session->set_flashdata('sukses','Anda Harus login dulu');
			redirect(base_url('login'),'refresh');
		}
		
		$data = array('id_link' => $id_link);
		$this->link_model->hapus($data);
			$this->session->set_flashdata('sukses','Data berhasil dihapus');
			redirect(base_url('admin/link'),'refresh');
	}

} 