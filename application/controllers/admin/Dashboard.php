<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('MCardview');
    }
	//halaman utama
	public function index()
	{	

		$anggota = $this->MCardview->anggota_aktif();
		$jumlahbuku = $this->MCardview->jumlah_buku();
		$PHini = $this->MCardview->PHini();
		$PHKini = $this->MCardview->PHKini();
		$data =  array('title' => 'Dashboard Admin' ,
					   'konten' => 'sadmin/dashboard/isi' ,
					   'anggotaaktif' => $anggota,
					   'Jbuku' => $jumlahbuku,
					   'PHini' => $PHini,
					   'PHKini' => $PHKini

					);

		$this->load->view('sadmin/layout/wrapper', $data, FALSE);

    }

    //halaman profil
    public function profil(){
    	$id_user = $this->session->userdata('id_user');
    	$user = $this->user_model->detail($id_user); 

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('email','Email','required|valid_email',
				array('required' => 'email harus di isi',
					  'valid_email' => 'Format email anda salah'));

		$valid->set_rules('nama','Nama','required',
				array('required' => 'nama harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Update profil '.$user ->nama,
						'user' =>$user,
					   'konten' => 'sadmin/dashboard/profil');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;

			//password di ganti jika diisi lebih dari 8 karakter
			if(strlen($i->post('password'))>8){

				$data = array ( 'id_user'			=> $id_user,
								'nama'				=> $i->post('nama'),
								'email'				=> $i->post('email'),
								'password'			=> sha1($i->post('password')),
								'akses_level'		=> $i->post('akses_level'),
								'keterangan'		=> $i->post('keterangan')
				);

			} else {

				$data = array ( 'id_user'			=> $id_user,
								'nama'				=> $i->post('nama'),
								'email'				=> $i->post('email'),
								'akses_level'		=> $i->post('akses_level'),
								'keterangan'		=> $i->post('keterangan')
				);
			}

			$this->user_model->edit($data);
			$this->session->set_flashdata('sukses','Profil berhasil diupdate');
			redirect(base_url('admin/dashboard/profil'),'refresh');
		}
	}
}