<?php
Class Bahasa extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('bahasa_model');
	}

	//halaman utama
	public function index (){
		$bahasa = $this->bahasa_model->listing();

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('kode_bahasa','Kode Bahasa Buku','required|is_unique[bahasa.kode_bahasa]',
				array('required' => 'Kode Bahasa Buku harus di isi',
					  'is_unique' => 'Kode Bahasa Buku sudah di gunakan'));

		$valid->set_rules('nama_bahasa','Nama_bahasa','required',
				array('required' => 'Nama bahasa harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Kelola Bahasa Buku ('.count($bahasa).')',
						'bahasa' => $bahasa,
					   'konten' => 'sadmin/bahasa/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;
			$data = array ( 'kode_bahasa'		=> $i->post('kode_bahasa'),
							'nama_bahasa'		=> $i->post('nama_bahasa'),							
							'keterangan'		=> $i->post('keterangan'),
							'urutan'			=> $i->post('urutan')
			);
			$this->bahasa_model->tambah($data);
			$this->session->set_flashdata('sukses','Data berhasil ditambah');
			redirect(base_url('admin/bahasa'),'refresh');
		}
	}

	//halaman edit bahasa
	public function edit ($id_bahasa){
		$bahasa = $this->bahasa_model->detail($id_bahasa); 

		//validasi
		$valid = $this->form_validation;


		$valid->set_rules('nama_bahasa','Nama_bahasa','required',
				array('required' => 'Nama bahasa harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Edit Bahasa '.$bahasa ->nama_bahasa,
						'bahasa' =>$bahasa,
					   'konten' => 'sadmin/bahasa/edit');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;

				$data = array ( 'id_bahasa'			=> $id_bahasa,
								'kode_bahasa'		=> $i->post('kode_bahasa'),
								'nama_bahasa'		=> $i->post('nama_bahasa'),						
								'keterangan'		=> $i->post('keterangan'),
								'urutan'			=> $i->post('urutan')
				);

			$this->bahasa_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil diupdate');
			redirect(base_url('admin/bahasa'),'refresh');
		}
	}

	//hapus data bahasa
	public function hapus($id_bahasa){
		//perlindungan
		if ($this->session->userdata('useraname')== "" && $this->session->userdata('akses_level')=="") {
			$this->session->set_flashdata('sukses','Anda Harus login dulu');
			redirect(base_url('login'),'refresh');
		}
		
		$data = array('id_bahasa' => $id_bahasa);
		$this->bahasa_model->hapus($data);
			$this->session->set_flashdata('sukses','Data berhasil dihapus');
			redirect(base_url('admin/bahasa'),'refresh');
	}

} 