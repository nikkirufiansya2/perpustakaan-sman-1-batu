<?php
Class Anggota extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('anggota_model');
	}
	
	//halaman utama anggota
	public function index (){
		$anggota = $this->anggota_model->listing();

		$data = array ('title' => 'Data Anggota ('.count($anggota).')',
						'anggota' => $anggota,
					   'konten' => 'sadmin/anggota/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
	}

	//halaman tambah anggota
	public function tambah (){

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('username','Username','required|is_unique[anggota.username]',
				array('required' => 'Username harus di isi',
					  'is_unique' => 'Username sudah di gunakan'));

		$valid->set_rules('password','Password','required|min_length[8]',
				array('required' => 'password harus di isi',
					  'min_length' => 'Password minimal 8 karakter'));

		$valid->set_rules('email','Email','required|valid_email',
				array('required' => 'email harus di isi',
					  'valid_email' => 'Format email anda salah'));

		$valid->set_rules('nama_anggota','Nama','required',
				array('required' => 'nama_anggota harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Tambah Anggota',
					   'konten' => 'sadmin/anggota/tambah');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;
			$data = array ( 'id_user'			=> $this->session->userdata('id_user'),
							'status_anggota'	=> $i->post('status_anggota'),
							'nama_anggota'		=> $i->post('nama_anggota'),
							'email'				=> $i->post('email'),
							'telepon'			=> $i->post('telepon'),
							'instansi'			=> $i->post('instansi'),
							'username'			=> $i->post('username'),
							'password'			=> sha1($i->post('password')) 							
			);
			$this->anggota_model->tambah($data);
			$this->session->set_flashdata('sukses','Data berhasil ditambah');
			redirect(base_url('admin/anggota'),'refresh');
		}
	}

	//halaman edit anggota
	public function edit ($id_anggota){
		$anggota = $this->anggota_model->detail($id_anggota); 

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('email','Email','required|valid_email',
				array('required' => 'email harus di isi',
					  'valid_email' => 'Format email anda salah'));

		$valid->set_rules('nama_anggota','Nama','required',
				array('required' => 'nama_anggota harus di isi'));

		if($valid->run()===FALSE){

		$data = array ('title' => 'Edit Anggota '.$anggota ->nama_anggota,
						'anggota' =>$anggota,
					   'konten' => 'sadmin/anggota/edit');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}else{
			$i= $this->input;

			//password di ganti jika diisi lebih dari 8 karakter
			if(strlen($i->post('password'))>8){

				$data = array ( 'id_anggota'		=> $id_anggota,
								'id_user'			=> $this->session->userdata('id_user'),
								'status_anggota'	=> $i->post('status_anggota'),
								'nama_anggota'		=> $i->post('nama_anggota'),
								'email'				=> $i->post('email'),
								'telepon'			=> $i->post('telepon'),
								'instansi'			=> $i->post('instansi'),
								'password'			=> sha1($i->post('password'))
				);

			} else {

				$data = array ( 'id_anggota'		=> $id_anggota,
								'id_user'			=> $this->session->userdata('id_user'),
								'status_anggota'	=> $i->post('status_anggota'),
								'nama_anggota'		=> $i->post('nama_anggota'),
								'email'				=> $i->post('email'),
								'telepon'			=> $i->post('telepon'),
								'instansi'			=> $i->post('instansi'),
				);
			}

			$this->anggota_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil diupdate');
			redirect(base_url('admin/anggota'),'refresh');
		}
	}

	//hapus data anggota
	public function hapus($id_anggota){
		//perlindungan
		if ($this->session->userdata('username')== "" && $this->session->userdata('akses_level')=="") {
			$this->session->set_flashdata('sukses','Anda Harus login dulu');
			redirect(base_url('login'),'refresh');
		}
		
		$data = array('id_anggota' => $id_anggota);
		$this->anggota_model->hapus($data);
			$this->session->set_flashdata('sukses','Data berhasil dihapus');
			redirect(base_url('admin/anggota'),'refresh');
	}

}