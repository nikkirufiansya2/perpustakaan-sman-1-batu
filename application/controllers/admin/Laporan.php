<?php  
	class Laporan extends CI_Controller
	{
		
		public function __construct(){
			parent::__construct();
			$this->load->model('Laporan_model');
			$this->load->model('peminjaman_model');
		}

		public function laporan_anggota_aktif()
		{
			$laporan_anggota_aktif = $this->Laporan_model->listingAnggotaAktif();
			
			$data = array('title' => 'Data Anggota Aktif ('.count($laporan_anggota_aktif).')',
				'laporan_anggota_aktif' => $laporan_anggota_aktif,
				'konten' => 'sadmin/laporan/anggota_aktif/isi');
			$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		}

		public function cetak_anggota_aktif()
		{
			$laporan_anggota_aktif['anggota_aktif'] = $this->Laporan_model->listingAnggotaAktif();
			$this->load->library('pdf');
			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->filename = "laporan-data-anggota_aktif.pdf";
			$this->pdf->load_view('sadmin/laporan/anggota_aktif/laporan_anggota_aktif', $laporan_anggota_aktif);
		}
		public function laporan_data_peminjaman_buku()
		{
			$peminjaman = $this->peminjaman_model->listing();
			$data = array ('title'		=> 'Data Peminjaman ('.count($peminjaman).')',
				'peminjaman'=> $peminjaman,
				'konten' => 'sadmin/laporan/peminjaman/isi'
			);
			$this->load->view('sadmin/layout/wrapper',$data,FALSE); 
		}

		public function cetak_data_peminjaman()
		{
			$peminjaman['peminjaman'] = $this->peminjaman_model->listing();
			$this->load->library('pdf');
			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->filename = "laporan-data-anggota_aktif.pdf";
			$this->pdf->load_view('sadmin/laporan/peminjaman/laporan_data_peminjaman', $peminjaman);
		}

		public function laporan_data_trend_buku()
		{
			$laporan_trend_buku = $this->Laporan_model->trend_buku();
			$data = array ('title'		=> 'Data tren Buku ('.count($laporan_trend_buku).')',
				'laporan_trend_buku'=> $laporan_trend_buku,
				'konten' => 'sadmin/laporan/trend_buku/isi'
			);
			$this->load->view('sadmin/layout/wrapper',$data,FALSE); 
		}

		public function cetak_data_trenBuku()
		{
			$laporan_trend_buku['trend_buku'] = $this->Laporan_model->trend_buku();
			$this->load->library('pdf');
			$this->pdf->setPaper('A4', 'potrait');
			$this->pdf->filename = "laporan-data-tren-buku.pdf";
			$this->pdf->load_view('sadmin/laporan/trend_buku/laporan_data_trend_buku', $laporan_trend_buku);
			//echo json_encode($laporan_trend_buku);
		}
	}
	?>