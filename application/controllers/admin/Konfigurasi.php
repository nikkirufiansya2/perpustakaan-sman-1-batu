<?php
/**
 * 
 */
class Konfigurasi extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->model('konfigurasi_model');
		//proteksi halaman
		if ($this->session->userdata('akses_level') != "admin") {
			$this->session->set_flashdata('sukses', 'Anda Harus Login Dulu !');
			redirect(base_url('login'), 'refresh');
		}
	}
	//main page konfigurasi
	public function index (){
		$konfigurasi = $this->konfigurasi_model->listing();

		//validasi
		$this->form_validation->set_rules('namaweb','Nama Web', 'required',
				array(	'required'	=> 'Nama Website harus diisi'));

		if($this->form_validation->run()=== FALSE) {
			//end validasi

		$data = array(	'title'			=> 'Konfigurasi Website : '.$konfigurasi->namaweb,
						'konfigurasi'	=> $konfigurasi,
						'konten'		=> 'sadmin/konfigurasi/isi'
					);
		$this->load->view('sadmin/layout/wrapper', $data, FALSE);
		//proses masuk database
		} else {
			$i 		= $this->input;
			$data 	= array(	'id_konfigurasi'			=> $konfigurasi->id_konfigurasi,
								'id_user'					=> $this->session->userdata('id_user'),
								'namaweb'					=> $i->post('namaweb'),
								'tagline'					=> $i->post('tagline'),
								'deskripsi'					=> $i->post('deskripsi'),
								'keywords'					=> $i->post('keywords'),
								'email'						=> $i->post('email'),
								'website'					=> $i->post('website'),
								'facebook'					=> $i->post('facebook'),
								'twitter'					=> $i->post('twitter'),
								'instagram'					=> $i->post('instagram'),
								'map'						=> $i->post('map'),
								'metatext'					=> $i->post('metatext'),
								'telepon'					=> $i->post('telepon'),
								'alamat'					=> $i->post('alamat'),
								'max_hari_peminjaman'		=> $i->post('max_hari_peminjaman'),
								'max_jumlah_peminjaman'		=> $i->post('max_jumlah_peminjaman'),
								'denda_perhari'				=> $i->post('denda_perhari')
							);
			$this->konfigurasi_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil di uupdate');
			redirect (base_url('admin/konfigurasi'),'refresh'); 
		}
		//akhir masuk database
	}
	//konfigurasi logo
	public function logo() {
		$konfigurasi = $this->konfigurasi_model->listing();

		//validasi
		$this->form_validation->set_rules('id_konfigurasi','ID Konfigurasi', 'required',
				array(	'required'	=> 'ID Konfigurasi harus diisi'));

		if($this->form_validation->run()) {
			$config['upload_path']   = './assets/upload/image/';
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size']      = '12000'; // KB  
			$this->upload->initialize($config);
			if(! $this->upload->do_upload('logo')) {
			//end validasi


		$data = array(	'title'			=> 'Konfigurasi Logo Website : '.$konfigurasi->namaweb,
						'konfigurasi'	=> $konfigurasi,
						'error'			=> $this->upload->display_errors(),
						'konten'		=> 'sadmin/konfigurasi/logo'
					);
		$this->load->view('sadmin/layout/wrapper', $data, FALSE);
		//proses masuk database
		} else {

			//UPLOAD gambar
			$upload_data        		= array('uploads' =>$this->upload->data());

			// Image Editor
			$config['image_library']  	= 'gd2';
			$config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name'];
			$config['new_image']     	= './assets/upload/image/thumbs/';
			$config['create_thumb']   	= TRUE;
			$config['quality']       	= "100%";
			$config['maintain_ratio']   = TRUE;
			$config['width']       		= 360; // Pixel
			$config['height']       	= 360; // Pixel
			$config['x_axis']       	= 0;
			$config['y_axis']       	= 0;
			$config['thumb_marker']   	= '';
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			//hapus logo lama
			if ($konfigurasi->logo != "") {				
				unlink('./assets/upload/image/'.$konfigurasi->logo);
				unlink('./assets/upload/image/thumbs/'.$konfigurasi->logo);
			}

			$i 		= $this->input;
			$data 	= array(	'id_konfigurasi'=> $konfigurasi->id_konfigurasi,
								'id_user'		=> $this->session->userdata('id_user'),
								'logo'			=> $upload_data['uploads']['file_name']
							);
			$this->konfigurasi_model->edit($data);
			$this->session->set_flashdata('sukses','Logo berhasil di uupdate');
			redirect (base_url('admin/konfigurasi/logo'),'refresh'); 
		}}
		//akhir masuk database
		$data = array(	'title'			=> 'Konfigurasi Logo Website : '.$konfigurasi->namaweb,
						'konfigurasi'	=> $konfigurasi,
						'konten'		=> 'sadmin/konfigurasi/logo'
					);
		$this->load->view('sadmin/layout/wrapper', $data, FALSE);
	}
	//konfigurasi ikon
	public function icon() {
		$konfigurasi = $this->konfigurasi_model->listing();

		//validasi
		$this->form_validation->set_rules('id_konfigurasi','ID Konfigurasi', 'required',
				array(	'required'	=> 'ID Konfigurasi harus diisi'));

		if($this->form_validation->run()) {
			$config['upload_path']   = './assets/upload/image/';
			$config['allowed_types'] = 'jpg|jpeg|gif|png';
			$config['max_size']      = '12000'; // KB  
			$this->upload->initialize($config);
			if(! $this->upload->do_upload('icon')) {
			//end validasi


		$data = array(	'title'			=> 'Konfigurasi icon Website : '.$konfigurasi->namaweb,
						'konfigurasi'	=> $konfigurasi,
						'error'			=> $this->upload->display_errors(),
						'konten'		=> 'sadmin/konfigurasi/icon'
					);
		$this->load->view('sadmin/layout/wrapper', $data, FALSE);
		//proses masuk database
		} else {

			//UPLOAD gambar
			$upload_data        		= array('uploads' =>$this->upload->data());

			// Image Editor
			$config['image_library']  	= 'gd2';
			$config['source_image']   	= './assets/upload/image/'.$upload_data['uploads']['file_name'];
			$config['new_image']     	= './assets/upload/image/thumbs/';
			$config['create_thumb']   	= TRUE;
			$config['quality']       	= "100%";
			$config['maintain_ratio']   = TRUE;
			$config['width']       		= 360; // Pixel
			$config['height']       	= 360; // Pixel
			$config['x_axis']       	= 0;
			$config['y_axis']       	= 0;
			$config['thumb_marker']   	= '';
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			
			//hapus logo lama
			if ($konfigurasi->icon != "") {				
				unlink('./assets/upload/image/'.$konfigurasi->icon);
				unlink('./assets/upload/image/thumbs/'.$konfigurasi->icon);
			}

			$i 		= $this->input;
			$data 	= array(	'id_konfigurasi'=> $konfigurasi->id_konfigurasi,
								'id_user'		=> $this->session->userdata('id_user'),
								'icon'			=> $upload_data['uploads']['file_name']
							);
			$this->konfigurasi_model->edit($data);
			$this->session->set_flashdata('sukses','icon berhasil di uupdate');
			redirect (base_url('admin/konfigurasi/icon'),'refresh'); 
		}}
		//akhir masuk database
		$data = array(	'title'			=> 'Konfigurasi icon Website : '.$konfigurasi->namaweb,
						'konfigurasi'	=> $konfigurasi,
						'konten'		=> 'sadmin/konfigurasi/icon'
					);
		$this->load->view('sadmin/layout/wrapper', $data, FALSE);
	}
}