<?php
Class File_buku extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('buku_model');
		$this->load->model('file_buku_model');
	}
	
	//halaman utama file_buku
	public function index (){
		$file_buku = $this->file_buku_model->listing();

		$data = array ('title' 		=> 'Data File Buku ('.count($file_buku).')',
						'file_buku' => $file_buku,
					   'konten' 	=> 'sadmin/file_buku/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
	}

	public function download ($id_file_buku){
		$file_buku = $this->file_buku_model->detail($id_file_buku);
		//proses download
		$folder = './assets/upload/files/';
		$file = $file_buku->nama_file;
		force_download($folder.$file, NULL);
	}

	//kelola file buku
	public function kelola ($id_buku){
		$file_buku = $this->file_buku_model->buku($id_buku);
		$buku 	= $this->buku_model->detail($id_buku);

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('judul_file','Judul File','required',
				array('required' => 'Judul File harus di isi'));

		if($valid->run()){
			$config['upload_path']   = './assets/upload/files/';
			$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
			$config['max_size']      = '12000'; // KB  
			$this->upload->initialize($config);
			if(! $this->upload->do_upload('nama_file')) {

		$data = array ('title' 		=> 'Data File Buku '.$buku->judul_buku.' ('.count($file_buku).')',
						'file_buku' => $file_buku,
						'buku'		=> $buku,
						'error'		=> $this->upload->display_errors(),
					   'konten' 	=> 'sadmin/file_buku/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		//masuk database
		}else{
			//UPLOAD COVER
			$upload_data        		= array('uploads' =>$this->upload->data());
			
			$i= $this->input;
			$data = array ( 'id_user'			=> $this->session->userdata('id_user'),
							'id_buku'			=> $id_buku,
							'judul_file'		=> $i->post('judul_file'),
							'nama_file'			=> $upload_data['uploads']['file_name'],
							'keterangan'		=> $i->post('keterangan'),
							'urutan'			=> $i->post('urutan')						
			);
			$this->file_buku_model->tambah($data);
			$this->session->set_flashdata('sukses','Data berhasil ditambah');
			redirect(base_url('admin/file_buku/kelola/'.$id_buku),'refresh');
		}}
		//end masuk databse
		$data = array ('title' 		=> 'Data File Buku '.$buku->judul_buku.' ('.count($file_buku).')',
						'file_buku' => $file_buku,
						'buku'		=> $buku,
					   'konten' 	=> 'sadmin/file_buku/isi');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);

	}


	//halaman edit file_buku
	public function edit ($id_file_buku){
		$file_buku = $this->file_buku_model->detail($id_file_buku);
		$id_buku = $file_buku->id_buku;
		$buku 	= $this->buku_model->detail($id_buku);

		//validasi
		$valid = $this->form_validation;

		$valid->set_rules('judul_file','Judul File','required',
				array('required' => 'Judul File harus di isi'));

		if($valid->run()){
			if(!empty($_FILES['nama_file']['name'])) {
			$config['upload_path']   = './assets/upload/files/';
			$config['allowed_types'] = 'pdf|doc|docx|xls|xlsx|ppt|pptx';
			$config['max_size']      = '12000'; // KB  
			$this->upload->initialize($config);
			if(! $this->upload->do_upload('nama_file')) {

		$data = array ('title' 		=> 'Edit File Buku '.$buku->judul_buku.' ('.count($file_buku).')',
						'file_buku' => $file_buku,
						'buku'		=> $buku,
						'error'		=> $this->upload->display_errors(),
					   'konten' 	=> 'sadmin/file_buku/edit');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);
		//masuk database
		}else{
			//UPLOAD file
			$upload_data        		= array('uploads' =>$this->upload->data());

			//hapus file lama kalo ada upload file baru
			unlink('./assets/upload/files/'.$file_buku->nama_file);

			$i= $this->input;
			$data = array ( 'id_file_buku'		=> $id_file_buku,
							'id_user'			=> $this->session->userdata('id_user'),
							'id_buku'			=> $id_buku,
							'judul_file'		=> $i->post('judul_file'),
							'nama_file'			=> $upload_data['uploads']['file_name'],
							'keterangan'		=> $i->post('keterangan'),
							'urutan'			=> $i->post('urutan')						
			);
			$this->file_buku_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil diupdate');
			redirect(base_url('admin/file_buku/kelola/'.$id_buku),'refresh');
		}} else {
			$i= $this->input;
			$data = array ( 'id_file_buku'		=> $id_file_buku,
							'id_user'			=> $this->session->userdata('id_user'),
							'id_buku'			=> $id_buku,
							'judul_file'		=> $i->post('judul_file'),
							'keterangan'		=> $i->post('keterangan'),
							'urutan'			=> $i->post('urutan')						
			);
			$this->file_buku_model->edit($data);
			$this->session->set_flashdata('sukses','Data berhasil diupdate');
			redirect(base_url('admin/file_buku/kelola/'.$id_buku),'refresh');
		}}
		//end masuk databse
		$data = array ('title' 		=> 'Edit File Buku '.$buku->judul_buku.' ('.count($file_buku).')',
						'file_buku' => $file_buku,
						'buku'		=> $buku,
					   'konten' 	=> 'sadmin/file_buku/edit');
		$this->load->view('sadmin/layout/wrapper',$data, FALSE);

	}
	

	//hapus data file_buku
	public function hapus($id_file_buku,$id_buku){
		//perlindungan
		if ($this->session->userdata('username')== "" && $this->session->userdata('akses_level')=="") {
			$this->session->set_flashdata('sukses','Anda Harus login dulu');
			redirect(base_url('login'),'refresh');
		}

		//hapus file
		$file_buku = $this->file_buku_model->detail($id_file_buku);

		if($file_buku->nama_file != "") {
			unlink('./assets/upload/files/'.$file_buku->nama_file);
		}
		
		$data = array('id_file_buku' => $id_file_buku);
		$this->file_buku_model->hapus($data);
			$this->session->set_flashdata('sukses','Data berhasil dihapus');
			redirect(base_url('admin/file_buku/kelola/'.$id_buku),'refresh');
	}

}