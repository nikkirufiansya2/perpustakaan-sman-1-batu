<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
		parent::__construct();
		$this->load->model("user_model");
    }
    //halaman login
    function index() {
        
    	//validasi
    	$valid= $this->form_validation;

    	$valid->set_rules('username','Username','required',
    			array('required'=>'username harus di isi'));

    	$valid->set_rules('password','Password','required|min_length[8]',
    			array('required'=>'password harus di isi',
    				   'min_length'=>'password minimal 8 karakter'));

    	if($valid ->run() === FALSE){
    		//akhir validasi
    	    $data = array ('title' => 'Login Perpustakan');
    	    $this->load->view("sadmin/login", $data);


    	//cek username dan password di database
    	} else {
    		$i 				= $this->input;
    		$username     	= $i->post('username');
    		$password     	= $i->post('password');
    		$check_login	= $this->user_model->login($username,$password);
        $check_login_anggota = $this->user_model->loginAnggota($username,$password);
    		//kalau ada data maka di arahkan ke dashboard
    		// if(count($check_login)== 1){
    		// 	$this->session->set_userdata('username', $username);
    		// 	$this->session->set_userdata('akses_level', $check_login->akses_level);
    		// 	$this->session->set_userdata('id_user', $check_login->id_user);
    		// 	$this->session->set_userdata('nama', $check_login->nama);
    		// 	redirect(base_url('admin/dashboard'),'refresh');
      //       } else if (count($check_login)== 2){
      //           $this->session->set_userdata('username', $username);
      //           $this->session->set_userdata('akses_level', $check_login->akses_level);
      //           $this->session->set_userdata('id_user', $check_login->id_user);
      //           $this->session->set_userdata('nama', $check_login->nama);
      //           redirect(base_url('home'),'refresh');

    		// }else{
    		// 	//username dan password tidak ada maka error
    		// 	$this->session->set_flashdata('sukses','usernama dan password tidak ada');
    		// 	redirect(base_url('login'),'refresh');
    		// }

            if (count($check_login) == 1){
               $this->session->set_userdata('username', $username);
               $this->session->set_userdata('akses_level', $check_login->akses_level);
               $this->session->set_userdata('id_user', $check_login->id_user);
               $this->session->set_userdata('nama', $check_login->nama);
               redirect(base_url('admin/dashboard'),'refresh');
            }
            //login anggota
            if (count($check_login_anggota) == 1) {
              $this->session->set_userdata('username', $username);
               $this->session->set_userdata('akses_level', $check_login_anggota->akses_level);
               $this->session->set_userdata('id_user', $check_login_anggota->id_anggota);
               $this->session->set_userdata('nama', $check_login_anggota->nama);
               redirect('home/homeUser');
            }
    	} //akhir cek login
    }


    //logout
    public function logout(){
        $this->session->unset_userdata('username');
        $this->session->unset_userdata('akses_level');
        $this->session->unset_userdata('id_user');
        $this->session->unset_userdata('nama');
        $this->session->set_flashdata('sukses', 'Anda berhasil keluar');
        redirect(base_url('login'),'refresh');
    }
}

  //   function auth() {
  //       $user = $this->input->post("username");
		// $pass = sha1($this->input->post("password"));

		// $result = $this->LoginModel->Verification($user,$pass);
		// if($result != []) {
		// 	$iduser = $result->id;
		// 	$nama = $result->nama;
		// 	$role = $result->status;  

		// 	$this->SessionModel->StoreSession($iduser,$role,$nama);
		// 	redirect("dashboard");
		// } else {
		// 	redirect("login");
		// }
  //   }
  