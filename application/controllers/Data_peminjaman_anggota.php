<?php
	/**
	 * 
	 */
	class Data_Peminjaman_anggota extends CI_Controller
	{
		
		public function __construct(){
			parent::__construct();
			$this->load->model('buku_model');
			$this->load->model('file_buku_model');
			$this->load->model('peminjaman_model');
			$this->load->model('Denda_Model');
		}

		public function index()
		{
			$id_anggota    = $this->session->userdata('id_user');
			$denda = $this->Denda_Model->listing();
			$buku = $this->peminjaman_model->peminjamanByUser($id_anggota);
			$data = array(	'title'		=>'Data Buku yang di pinjam',
				'peminjaman'		=> $buku,
				'denda'			=> $denda,
				'konten'		=>'user_pinjam/isi');
			$this->load->view('layout/wrapper',$data,FALSE);
		}
	}

	?>