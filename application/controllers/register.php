<?php
Class Register extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('anggota_model');
  }
  
  //halaman utama anggota
  public function index (){
    
    $this->load->view('sadmin/register');
  }

  public function tambahuser (){

    //validasi
    $valid = $this->form_validation;

    $valid->set_rules('username','Username','required|is_unique[user.username]',
        array('required' => 'Username harus di isi',
            'is_unique' => 'Username sudah di gunakan'));

    $valid->set_rules('password','Password','required|min_length[8]',
        array('required' => 'password harus di isi',
            'min_length' => 'Password minimal 8 karakter'));

    $valid->set_rules('email','Email','required|valid_email',
        array('required' => 'email harus di isi',
            'valid_email' => 'Format email anda salah'));

    $valid->set_rules('nama','Nama','required',
        array('required' => 'nama harus di isi'));

    if($valid->run()===FALSE){

    $data = array ('title' => 'Tambah User',
             'konten' => 'sadmin/register');
    $this->load->view('sadmin/register',$data, FALSE);
    }else{
      $i= $this->input;
      $data = array ( 'nama'        => $i->post('nama'),
              'email'       => $i->post('email'),
              'username'      => $i->post('username'),
              'password'      => sha1($i->post('password')),
              'akses_level'   => 'user',
              'keterangan'    => ''
      );
      $this->user_model->tambah($data);
      $this->session->set_flashdata('sukses','Data berhasil ditambah');
      redirect(base_url('login'),'refresh');
    }
  }

}