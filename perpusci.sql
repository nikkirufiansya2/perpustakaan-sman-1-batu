-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Waktu pembuatan: 12 Agu 2020 pada 17.42
-- Versi server: 10.4.6-MariaDB
-- Versi PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `perpusci`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(2) NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', 'admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `status_anggota` enum('aktif','non aktif') NOT NULL,
  `nama_anggota` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `instansi` varchar(50) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `id_user`, `status_anggota`, `nama_anggota`, `email`, `telepon`, `instansi`, `username`, `password`, `tanggal`) VALUES
(3, 2, 'aktif', 'elia', 'elia@mail.com', '12121', 'MM', 'elia12', 'ca85f5b6053488908369ffc2615ed698e2169236', '2020-02-12 06:40:59'),
(4, 2, 'non aktif', 'Amin', 'amin@mail.com', '11213', 'STIKI', 'amin', 'c7c53675d410bd2e332579edc1e1a42afb3c6e78', '2020-02-13 03:39:56'),
(5, 8, 'aktif', 'nikki', 'nikki@mail.com', '0813213', 'Malang\r\n', 'nikki', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', '2020-08-08 11:48:29');

-- --------------------------------------------------------

--
-- Struktur dari tabel `bahasa`
--

CREATE TABLE `bahasa` (
  `id_bahasa` int(11) NOT NULL,
  `kode_bahasa` varchar(3) NOT NULL,
  `nama_bahasa` varchar(50) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `bahasa`
--

INSERT INTO `bahasa` (`id_bahasa`, `kode_bahasa`, `nama_bahasa`, `keterangan`, `urutan`, `tanggal`) VALUES
(3, '00a', 'Indonesiaku', 'aaa', 1, '2020-02-12 13:51:10'),
(4, '002', 'Batak', 'adsd', 2, '2020-02-12 13:51:53'),
(5, '000', 'Jawa', '', 10, '2020-02-13 03:39:26'),
(6, '123', 'English', '', 101, '2020-02-20 13:32:25');

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `slug_berita` varchar(255) NOT NULL,
  `judul_berita` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `gambar` varchar(255) DEFAULT NULL,
  `status_berita` enum('Publish','Draft') NOT NULL,
  `jenis_berita` enum('Berita','Slider') NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `id_user`, `slug_berita`, `judul_berita`, `isi`, `gambar`, `status_berita`, `jenis_berita`, `tanggal`) VALUES
(1, 2, 'selamat-datang-di-perpustakaan', 'Selamat datang di Perpustakaan', '<p>Deskripsi</p>', 'images_(1).jpg', 'Publish', 'Slider', '2020-02-27 16:14:35'),
(4, 2, 'lomba-menggambar', 'Lomba Menggambar', '<p>Deskripsi</p>', 'download_(2).jpg', 'Publish', 'Slider', '2020-02-27 18:24:51'),
(5, 2, 'selamat-hari-buku-sedunia', 'Selamat Hari Buku Sedunia', '<p><em>\"Hari Buku Sedunia, dikenal pula dengan Hari Buku dan Hak Cipta Sedunia &amp; Hari Buku Internasional, merupakan hari perayaan tahunan yang jatuh pada tanggal 23 April yang diadakan oleh UNESCO untuk mempromosikan peran membaca, penerbitan, &amp; hakcipta. #haribukusedunia #worldbookday,\" tulis Penerbit Republika.</em></p>\r\n<p><em>&ldquo;Semakin banyak kamu membaca, semakin banyak hal yang akan kamu ketahui. Semakin banyak kamu belajar, semakin banyak tempat yang akan kamu kunjungi.&rdquo;</em>&nbsp;&ndash; Dr. Seuss<br /><br /></p>', 'decorative-background-with-colored-books-clouds_23-2147606482.jpg', 'Publish', 'Berita', '2020-02-27 17:49:46'),
(6, 2, 'peluncuran-buku', 'Peluncuran buku', '<p>lorem ksldls kdasjdka aslkdna</p>', 'all.png', 'Publish', 'Slider', '2020-02-28 04:34:55'),
(7, 2, 'test-slider-2', 'Test slider 2', '<p>ihkjhkjh</p>', 'header.png', 'Publish', 'Slider', '2020-02-28 04:36:59'),
(10, 2, 'test-sliderku', 'Test sliderku', '<p>asda</p>', 'camera1.png', 'Publish', 'Slider', '2020-02-28 04:17:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `buku`
--

CREATE TABLE `buku` (
  `id_buku` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `id_bahasa` int(11) NOT NULL,
  `judul_buku` varchar(255) NOT NULL,
  `penulis_buku` varchar(255) NOT NULL,
  `subjek_buku` varchar(255) DEFAULT NULL,
  `letak_buku` varchar(50) DEFAULT NULL,
  `kode_buku` varchar(50) DEFAULT NULL,
  `kolasi` int(11) DEFAULT NULL,
  `penerbit` varchar(255) DEFAULT NULL,
  `tahun_terbit` year(4) DEFAULT NULL,
  `nomor_seri` varchar(50) DEFAULT NULL,
  `status_buku` enum('publish','not publish','missing') DEFAULT NULL,
  `ringkasan` text DEFAULT NULL,
  `cover_buku` varchar(255) DEFAULT NULL,
  `jumlah_buku` int(11) DEFAULT NULL,
  `tanggal_entri` datetime NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `buku`
--

INSERT INTO `buku` (`id_buku`, `id_user`, `id_jenis`, `id_bahasa`, `judul_buku`, `penulis_buku`, `subjek_buku`, `letak_buku`, `kode_buku`, `kolasi`, `penerbit`, `tahun_terbit`, `nomor_seri`, `status_buku`, `ringkasan`, `cover_buku`, `jumlah_buku`, `tanggal_entri`, `tanggal`) VALUES
(1, 2, 8, 3, 'Olahraga', 'elia', '', 'rak 1', '', 0, '', 0000, '', 'publish', '', 'WhatsApp_Image_2020-01-14_at_23_20_47_(1).jpeg', 0, '2020-02-20 16:35:00', '2020-02-28 05:11:49'),
(3, 2, 6, 6, 'Hukum internasional', 'yosi', 'sds', 'sds', '1232', 121, 'Gramedia', 2001, 'asda', 'publish', 'sds', 'WhatsApp_Image_2020-01-24_at_15_54_52(2).jpeg', 2, '2020-02-25 05:27:19', '2020-02-28 03:38:33'),
(4, 2, 8, 3, 'BIOGRAFI GUS DUR', 'Greg Barton', '', 'rak 1', 'KB-001', 80, 'Gramedia', 2019, '0001', 'publish', 'Menceritakan Kehidupan Tokoh Terkemuka yaitu Gusdur', 'GUSDUR.jpg', 50, '2020-02-27 17:59:15', '2020-02-28 05:11:35'),
(5, 2, 10, 3, 'Orang Orang Biasa', 'Andrea Hirata', '12', 'rak 2', 'KB-002', 85, 'Gramedia', 2020, '0002', 'publish', 'Bercerita tentang bagaiamana cara orang hidup dengan normal.', 'orang-orang-biasa-1.jpg', 50, '2020-02-27 18:20:38', '2020-02-28 05:37:18'),
(6, 2, 7, 3, 'Buku kesehatan', 'GG', '12', 'rak 2', '', 0, '', 0000, '', 'publish', 'lorem', 'stiki1.png', 0, '2020-02-28 04:36:01', '2020-02-28 05:37:40');

-- --------------------------------------------------------

--
-- Struktur dari tabel `denda`
--

CREATE TABLE `denda` (
  `id` int(11) NOT NULL,
  `denda` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `denda`
--

INSERT INTO `denda` (`id`, `denda`) VALUES
(1, 2000);

-- --------------------------------------------------------

--
-- Struktur dari tabel `file_buku`
--

CREATE TABLE `file_buku` (
  `id_file_buku` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `judul_file` varchar(255) NOT NULL,
  `nama_file` varchar(255) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `file_buku`
--

INSERT INTO `file_buku` (`id_file_buku`, `id_user`, `id_buku`, `judul_file`, `nama_file`, `keterangan`, `urutan`, `tanggal`) VALUES
(1, 2, 1, 'BAB 3', '3_143310006_BAB_II.pdf', ' bahasa indonesia', 3, '2020-02-25 04:07:14'),
(4, 2, 1, 'Daftar pustaka', 'Publikasi_08_12_3121.pdf', 'dapus', 4, '2020-02-25 04:24:34'),
(5, 2, 3, 'Sampul', 'citra8d.ppt', ' as', 1, '2020-02-25 04:27:52'),
(6, 5, 5, 'Orang Orang Biasa', 'UNSUR_KRIMINALITAS_DALAM_NOVEL_ORANG-ORANG_BIASA_K.pdf', ' Unsur Kriminalitas Orang Orang Biasa', 1, '2020-02-27 19:58:05'),
(7, 5, 5, 'FILE SMBRANG', '4210-Article_Text-2734-1-10-20160808.pdf', ' asdasdasda', 2, '2020-02-27 20:09:45'),
(8, 2, 6, 'BAB 1', 'IT_Data-Mining.pdf', ' Bab 1 bahasa indonesia', 1, '2020-02-28 06:08:46');

-- --------------------------------------------------------

--
-- Struktur dari tabel `jenis`
--

CREATE TABLE `jenis` (
  `id_jenis` int(11) NOT NULL,
  `kode_jenis` varchar(20) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL,
  `keterangan` text DEFAULT NULL,
  `urutan` int(11) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `jenis`
--

INSERT INTO `jenis` (`id_jenis`, `kode_jenis`, `nama_jenis`, `keterangan`, `urutan`, `tanggal`) VALUES
(6, '0s02', 'Hukum', 'asda', 3, '2020-02-13 02:22:58'),
(7, '002', 'Teknologi', 'asa', 2, '2020-02-28 04:52:33'),
(8, '001', 'Fiksi', '', 1, '2020-02-28 04:42:44'),
(9, 'KJB-001', 'Biografi', 'kisah atau keterangan tentang kehidupan seseorang.', 4, '2020-02-28 04:52:47'),
(10, 'KJB-002', 'Novel', 'sebuah karya fiksi prosa yang tertulis dan naratif; biasanya dalam bentuk cerita', 5, '2020-02-27 17:18:42');

-- --------------------------------------------------------

--
-- Struktur dari tabel `konfigurasi`
--

CREATE TABLE `konfigurasi` (
  `id_konfigurasi` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `namaweb` varchar(255) NOT NULL,
  `tagline` varchar(255) DEFAULT NULL,
  `deskripsi` varchar(500) DEFAULT NULL,
  `keywords` varchar(500) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `website` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `facebook` varchar(255) DEFAULT NULL,
  `twitter` varchar(255) DEFAULT NULL,
  `instagram` varchar(255) DEFAULT NULL,
  `map` text DEFAULT NULL,
  `metatext` text DEFAULT NULL,
  `telepon` varchar(50) DEFAULT NULL,
  `alamat` varchar(300) DEFAULT NULL,
  `max_hari_peminjaman` int(11) DEFAULT NULL,
  `max_jumlah_peminjaman` int(11) DEFAULT NULL,
  `denda_perhari` int(11) DEFAULT NULL,
  `tanggal_update` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `konfigurasi`
--

INSERT INTO `konfigurasi` (`id_konfigurasi`, `id_user`, `namaweb`, `tagline`, `deskripsi`, `keywords`, `email`, `website`, `logo`, `icon`, `facebook`, `twitter`, `instagram`, `map`, `metatext`, `telepon`, `alamat`, `max_hari_peminjaman`, `max_jumlah_peminjaman`, `denda_perhari`, `tanggal_update`) VALUES
(1, 2, 'SI Perpustakaan Online SMA N 1 Batu', 'cari buku dengan mudah', '    Deskripsi Website', '    Keywords Website', 'sma1batu@mail.com', 'http://localhost/phpmyadmin/#PMAURL-13:tbl_addfield.php?db=perpusci&table=konfigurasi&server=1&target=&token=4b9dfb8e7c424f3e3933cbe3ca837202', 'logo_kemhas.png', 'absenn.png', 'https://web.whatsapp.com/', 'https://web.whatsapp.com/', 'https://web.whatsapp.com/', '    Google Maps (format iframe)', '    Metatext ( Biasanya dari Google analitys & Webmaster )', '12131312', '   Jalan Malang nomor 1 Kota Malang', 14, 2, 2000, '2020-03-05 03:30:05');

-- --------------------------------------------------------

--
-- Struktur dari tabel `link`
--

CREATE TABLE `link` (
  `id_link` int(11) NOT NULL,
  `nama_link` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `target` varchar(20) DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `link`
--

INSERT INTO `link` (`id_link`, `nama_link`, `url`, `target`, `tanggal`) VALUES
(1, 'STIKI Malang', 'http://perpustakaan.stiki.ac.id/', '_self', '2020-03-05 04:57:22');

-- --------------------------------------------------------

--
-- Struktur dari tabel `peminjaman`
--

CREATE TABLE `peminjaman` (
  `id_peminjaman` int(11) NOT NULL,
  `id_buku` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `tanggal_pinjam` date NOT NULL,
  `tanggal_kembali` date NOT NULL,
  `keterangan` text NOT NULL,
  `status_kembali` enum('belum','sudah','hilang') NOT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `peminjaman`
--

INSERT INTO `peminjaman` (`id_peminjaman`, `id_buku`, `id_anggota`, `id_user`, `tanggal_pinjam`, `tanggal_kembali`, `keterangan`, `status_kembali`, `tanggal`) VALUES
(5, 4, 4, 8, '2020-03-06', '2020-03-20', '', 'sudah', '2020-08-12 14:03:41'),
(6, 5, 5, 2, '2020-08-06', '2020-08-08', 'hij', 'sudah', '2020-08-06 00:04:50'),
(7, 5, 3, 2, '2020-08-06', '2020-08-08', 'dsfgfs', 'belum', '2020-08-06 00:15:02'),
(8, 5, 5, 2, '2020-08-06', '2020-08-08', '12313', 'belum', '2020-08-06 00:15:32');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(64) NOT NULL,
  `akses_level` varchar(20) NOT NULL,
  `foto` varchar(255) DEFAULT NULL,
  `keterangan` text DEFAULT NULL,
  `tanggal` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `nama`, `email`, `username`, `password`, `akses_level`, `foto`, `keterangan`, `tanggal`) VALUES
(1, 'admin', 'admin@mail.com', 'admin', '6c7ca345f63f835cb353ff15bd6c5e052ec08e7a', '1', NULL, NULL, '2020-02-05 17:00:00'),
(2, 'elia', 'elia@mail.com', 'elia', 'ca85f5b6053488908369ffc2615ed698e2169236', 'admin', NULL, '', '2020-02-11 17:24:39'),
(4, 'amin', 'amin@mail.com', 'amin', '85593210956e79f279506c83bf540d8f81095696', 'admin', NULL, '', '2020-02-13 03:42:55'),
(5, 'ZUPER', 'zuper@mail.com', 'mimin', 'f0911719715b9407c3c8c7bcc0dfc3ee337f9ace', 'admin', NULL, 'adeknya ADMIN', '2020-02-27 16:07:44'),
(7, 'amin', 'amin@mail.com', 'amin123', 'c7c53675d410bd2e332579edc1e1a42afb3c6e78', 'user', NULL, 'ada', '2020-02-28 03:53:43'),
(8, 'admin2', 'admin2@email.com', 'admin2', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 'admin', NULL, 'ok', '2020-08-06 12:16:06');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indeks untuk tabel `bahasa`
--
ALTER TABLE `bahasa`
  ADD PRIMARY KEY (`id_bahasa`),
  ADD UNIQUE KEY `kode_bahasa` (`kode_bahasa`,`nama_bahasa`);

--
-- Indeks untuk tabel `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`),
  ADD UNIQUE KEY `judul_berita` (`judul_berita`);

--
-- Indeks untuk tabel `buku`
--
ALTER TABLE `buku`
  ADD PRIMARY KEY (`id_buku`);

--
-- Indeks untuk tabel `denda`
--
ALTER TABLE `denda`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `file_buku`
--
ALTER TABLE `file_buku`
  ADD PRIMARY KEY (`id_file_buku`);

--
-- Indeks untuk tabel `jenis`
--
ALTER TABLE `jenis`
  ADD PRIMARY KEY (`id_jenis`),
  ADD UNIQUE KEY `kode_jenis` (`kode_jenis`,`nama_jenis`);

--
-- Indeks untuk tabel `konfigurasi`
--
ALTER TABLE `konfigurasi`
  ADD PRIMARY KEY (`id_konfigurasi`);

--
-- Indeks untuk tabel `link`
--
ALTER TABLE `link`
  ADD PRIMARY KEY (`id_link`);

--
-- Indeks untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  ADD PRIMARY KEY (`id_peminjaman`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`),
  ADD UNIQUE KEY `username` (`username`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `bahasa`
--
ALTER TABLE `bahasa`
  MODIFY `id_bahasa` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `buku`
--
ALTER TABLE `buku`
  MODIFY `id_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT untuk tabel `denda`
--
ALTER TABLE `denda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `file_buku`
--
ALTER TABLE `file_buku`
  MODIFY `id_file_buku` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `jenis`
--
ALTER TABLE `jenis`
  MODIFY `id_jenis` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `konfigurasi`
--
ALTER TABLE `konfigurasi`
  MODIFY `id_konfigurasi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `link`
--
ALTER TABLE `link`
  MODIFY `id_link` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `peminjaman`
--
ALTER TABLE `peminjaman`
  MODIFY `id_peminjaman` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;